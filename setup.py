from setuptools import setup,find_packages

setup(
    name='pylenstool',
    version='0.2',
    author='Guillaume MAHLER',
    author_email='guillaume.mahler@durham.ac.uk',
    install_requires=['numpy','astropy'],
    packages=['pylenstool'],
    include_package_data=True,
    package_data={'pylenstool':['default_param_file.par']},
    zip_safe=False
)


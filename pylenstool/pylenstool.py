import numpy as np
import os
import time
import copy
import math
import random
import sys
#input_filename = None

DEFAULTFILE=os.path.join(os.path.abspath(os.path.dirname(__file__)),'default_param_file.par')

class lenstool_param_file():
        core=[]
        comments=[]
        
        def __init__(self, param_file=DEFAULTFILE):
                core,comments=_set_model(param_file)
                
                self.core = core
                self.comments = comments
                self.input_filename=param_file 
                #global input_filename   
                #input_filename=best    
                #print best

        def get_inputfilename(self):
                """
                Returns the name of the initial parameter used 
                """
                return self.input_filename

        def get_comments(self):
                """
                Returns the list of comments
                """
                return self.comments
        def get_core(self):
                """
                Returns a list of list containings all section and parameters
                """
                return self.core
        def get_ref(self):
                """
                Returns a list containing the RA and DEC reference value used by the model
                """
                ref=['','']
                sec=0
                for i in range(0,len(self.core)):
                        if self.core[i][0][0]=='runmode':
                                sec=i
                for i in range(0,len(self.core[sec])):
                        if self.core[sec][i][0]=='reference':
                                ref[0]=self.core[sec][i][2]
                                ref[1]=self.core[sec][i][3]
                return ref
        def get_section(self):
                """
                Returns the name of all the sections as a list
                """
                only_section=[]
                for line in self.core:
                            only_section.append(line[0])
                return only_section

        def get_potential(self,pot_name,limit=False):
                """
                Returns the name of the content of a potential 

                Parameters
                ----------
                limit : bool     
                           Name of the region file 
                prefix_dwcs : string 
                              Name of the prefix of the outpulie dwcs file the routine iterate on the number of polygon.
                Returns
                ----------
                sec : list
                      Return the list of all the paramters of the section 
                limit: list 
                      If activatec return the list of all the paramters of the section 
                """
                sec=0
                limit_sec=0
                try:
                   only_section=[]
                   for line in self.core:
                     #print('PYLENSTOOL DEBUG:',line[0],len(line[0]))
                     if len(line[0])>1:
                       if line[0][0]=='potentiel' or line[0][0]=='potential':
                            #print('PYLENSTOOL DEBUG: pot name',pot_name,line[0][1])
                            if line[0][1]==pot_name:
                               #print('PYLENSTOOL DEBUG: Found it')
                               sec=line #.append(line[0])
                       if limit:
                          #print('PYLENSTOOL DEBUG: limit')
                          if line[0][0]=='limit':
                               if line[0][1]==pot_name:
                                  #print('PYLENSTOOL DEBUG: Found it')
                                  limit_sec=line
                              
                except:
                   print('NO potential have that name',pot_name)
                   print(get_section(self))
                   
                if limit:
                   return sec,limit_sec
                else:
                   return sec
        
        def get_parameter(self,section_name,param_name,verbose=True):
                """
                Returns the value(s) for a given parameter for a given section
                Becarefull, if two sections have the same name, the ouput will contain all of them into one list
                """
                value=0 
                if len(section_name.split())>1:       # That means it is a composite name like potentiel O1
                        a,b=section_name.split()      # I assume the section length = 2
                        for sect in self.core:        # I am looping over the file
                                if len(sect[0])>1:
                                        if sect[0][0]==a and sect[0][1]==b:
                                                for line in sect:
                                                        if line[0]==param_name:
                                                                if value==0:
                                                                        value=line
                                                                else:
                                                                        value=value+line
                                        
                else:
                        for sect in self.core:        # I am looping over the file
                                if sect[0][0]==section_name:    # I look for the section
                                        for line in sect:
                                                if line[0]==param_name:  # I look for the param
                                                        if value==0:
                                                                value=line
                                                        else:
                                                                value=value+line
                if verbose:
                        if value == 0 :
                                print('No parameter matching the ID section_name')
                return value

        def copy(self):
            """
            Returns a deep copy of the parameter file
            """
            pf_new=copy.deepcopy(self)
            return pf_new

                
        def get_zlens(self):
                """
                Returns a list of all the different redshifts of potential which are present in the model
                """
                rec_z_lens=[]
                for pot in self.get_section() :
                        if pot[0]=='potentiel' or pot[0]=='potential':
                                rec_z_lens.append(self.get_parameter(pot[0]+' '+pot[1],'z_lens')[1])
                        if pot[0][0:7]=='potfile':
                                rec_z_lens.append(self.get_parameter(pot[0],'zlens')[1])
                return sorted(set(rec_z_lens), key=rec_z_lens.index)
        
        def ds9regdeg2dwcs(self,name_reg='ds9.reg',prefix_dwcs='poly',verbose=False):
                """
                Transform a ds9 region file in a contour dwcs file (used in the section cleanlens contour)
                The ds9 region file need to be save in degrees and the contour need to be polygon (not ellispe, box or anything else)
        
                Parameters
                ----------
                name_reg : string     
                           Name of the region file 
                prefix_dwcs : string 
                              Name of the prefix of the outpulie dwcs file the routine iterate on the number of polygon.
                Returns
                ----------
                num : list
                      Return the list of all the output name file
                """
                ref=self.get_ref()
                RAref=float(ref[0])
                DECref=float(ref[1])
                cosDECref=np.cos(np.pi/180.0*DECref)
                name_output_list=[]
                with open(name_reg, 'r') as f:
                        num=0
                        for line in f:
                                if line[0:7]=='polygon' :
                                        poly=[]
                                        tmp=line.split('polygon(')[1].split(')')[0]
                                        splitted_line=tmp.split(',')
                                        try : # Does the polygon have a ID
                                             if verbose:
                                                print('The text of the polygon is:',line.split('text={')[1].split('}')[0])
                                             name_text=line.split('text={')[1].split('}')[0]
                                        except:
                                                name_text=''
                                        if verbose:
                                             print('There is {} vertices'.format(int(len(splitted_line)/2)))
                                        for i in range(0,int(len(splitted_line)/2)):
                                                poly.append([i,-3600*(float(splitted_line[2*i])-RAref)*cosDECref,3600*(float(splitted_line[2*i+1])-float(DECref))])
                                        
                                        if name_text!='':
                                                name_output=str(prefix_dwcs)+'_'+str(name_text)+'.dwcs'
                                        else :
                                                name_output=str(prefix_dwcs)+'_'+str(num)+'.dwcs'
                                        name_output_list.append(name_output)
                                        np.savetxt(name_output,poly,fmt='%i    %2.3f    %2.3f')
                                        num=num+1
                return name_output_list
       
        def dwcs2ds9regdeg(self,name_dwcs='default.dwcs',ds9_output_name='default',verbose=False):
                """
                Transform a .dwcs lenstool file in a ds9 region file
        
                Parameters
                ----------
                name_dwcs : string     
                            Name of the region file 
                prefix_dwcs : string 
                              Name of the prefix for the ds9 ouput name. default is "pylenstool_dwcs2ds9_"+name_dwcs+".reg"
                Returns
                ----------
                ds9_name_output : string
                                  Name of the written ds9 region file 
                      
                """
                ref=self.get_ref()
                RAref=float(ref[0])
                DECref=float(ref[1])
                cosDECref=np.cos(np.pi/180.0*DECref)
                name_output_list=[]
                print("This function as not finished")
                return ds9_name_output

 
        def clean(self,verbose=False):
                """
                Remove all runmode commands and do not make the model producing any critical or caustic line. 
                An hard list of known command is put in pylenstool (It might miss something. Please send a report if you find it).
                """
                runmode_param=['image','source','time','ampli','poten','mass','shear','shearfield','inverse','imseeing','grille','minchi0','pixel','verbose','marker','radialprop','dpl','restart','arclet']

                for param in runmode_param:
                        self.remove_parameter('runmode',param,verbose=verbose)
                self.set_parameter('cline','nplan 0',verbose=verbose)
                


        def remove_parameter(self,section,param,removeid=0,verbose=True):
                """
                Removes for a specified section the specified parameter.

                Parameters
                ----------
                section  : string
                          section name
                param    : string
                          param name
                removeid : int
                          Optional: ID of the Nieme occurence of the section that will be removed. By default the first occurence is removed
                """
                get_it=False #in case verbose is True it will tell you if you wanted to suppres an non-existing parameter
                a=[]
                runid=0
                for el in self.core:
                        if el[0][0]!=section:
                                a.append(el)
                        if el[0][0]==section:
                                new_el=[]
                                for sub_el in el:
                                        if sub_el[0]!=param:
                                                new_el.append(sub_el)
                                        else :
                                           if(removeid>0):
                                               runid=runid+1
                                               if(runid==removeid):
                                                   get_it=True
                                                   if verbose :
                                                        print('In section',section,'parameter number',str(removeid),' ',param,'has been removed')
                                               else:
                                                   new_el.append(sub_el)
                                           else:
                                               get_it=True
                                               if verbose :
                                                   print('In section',section,'the parameter',param,'has been removed')
                                a.append(new_el)
                if not get_it and verbose:
                        print('There were no parameter ',param,'in section ',section)
                self.core=a

        def remove_section(self,section_name,removeid=0,verbose=True):
                """
                Removes the section which given in argument.

                Parameters
                ----------
                section_name : string
                               name of the section that will be removed from the parameter file
                removeid     : int
                               Optional: ID of the Nieme occurence of the section that will be removed. By default the first occurence is removed
                """
                get_it=False #in case verbose is True it will tell you if you wanted to suppres an non-existing parameter
                a=[]
                runid=0
                for el in self.core:
                        if el[0][0]!=section_name:
                                a.append(el)
                        if el[0][0]==section_name:
                            if(removeid>0):
                                runid=runid+1
                                if(runid==removeid):
                                        get_it=True
                                        if verbose :
                                               print('The section ',section_name,' number ',removeid,' has been removed')
                                else:
                                        a.append(el)
                            else:
                                get_it=True
                                if verbose :
                                        print('The section {} has been removed'.format(section_name))

                if not get_it and verbose:
                       print('No section under this name: {} was found'.format(section_name))
                self.core=a

        def remove_potential(self,pot_to_keep,limit=False,verbose=True): 
                """
                Removes every potential which are in the list of IDs given in argument.
                If you want to keep the original parameter file and make a copy with the updated potentials,
                do not use this method but the function pylenstool.remove_potential(parameter_file,pot_to_keep)

                Parameters
                ----------
                pot_to_keep : list of string
                              list of potential names
                limit :       bool
                              remove limit section with same ID
                verbose :     bool
                              verbose activation
                """
                #print('DEBUG PYLENSTOOL:',pot_to_keep)
                a=[]
                if not limit:
                  #print('DEBUG PYLENSTOOL remove_potential:  I Should not be here',limit)
                  for el in self.core:
                        #print('DEBUG PYLENSTOOL:',el[0][0])
                        if el[0][0]!='potentiel' and  el[0][0]!='potential':
                                #print('DEBUG PYLENSTOOL:       should only be non-potential section',el[0][0],el[0][0]!='potential')
                                a.append(el)
                        if el[0][0]=='potentiel' or el[0][0]=='potential':
                                #print('DEBUG PYLENSTOOL:     el1',el[0][1])
                                to_be_kept=True
                                for pot_name in pot_to_keep:
                                        if el[0][1]==pot_name:
                                                to_be_kept=False
                                                #print('DEBUG PYLENSTOOL:    ',to_be_kept)
                                if to_be_kept: 
                                        a.append(el)
                if limit:
                  for el in self.core:
                        #print('DEBUG PYLENSTOOL:',el[0][0])
                        if el[0][0]!='potentiel' and  el[0][0]!='potential' and el[0][0]!='limit' :
                                #print('DEBUG PYLENSTOOL:       should only be non-potential section',el[0][0],el[0][0]!='potential')
                                a.append(el)
                        if el[0][0]=='potentiel' or el[0][0]=='potential' or el[0][0]=='limit':
                                #print('DEBUG PYLENSTOOL remove_potential:     el1',el[0][1])
                                to_be_kept=True
                                for pot_name in pot_to_keep:
                                        #print('DEBUG PYLENSTOOL remove_potential: pot_name',el[0][0],pot_name,el[0][1])
                                        if el[0][1]==pot_name:
                                                to_be_kept=False
                                                #print('DEBUG PYLENSTOOL remove_potential:    ',to_be_kept,a)
                                if to_be_kept:
                                        a.append(el)


                self.core=a
        def keep_potential(self,pot_to_keep):  
                """
                Removes every potential which not in the list of IDs given in argument.
                If you want to keep the parameter file and make a copy,
                use pylenstool.keep_potential(parameter_file,pot_to_keep)

                Parameters
                ----------
                pot_to_keep : list of string
                              list of potential names
                """
                a=[]
                for el in self.core:
                        if el[0][0]!='potentiel' and el[0][0]!='potential':
                                a.append(el)
                        if el[0][0]=='potentiel' or el[0][0]=='potential':
                                for pot_name in pot_to_keep:
                                        if el[0][1]==pot_name:
                                                a.append(el)
                self.core=a

        #The following section will be deleted in the futur
        def keep_only_some_potentials(self,pot_to_keep):   # We need to add a feature that removes also limit XX when the potential is removed
                """
                Removes every potential which not in the list of IDs given in argument.
                If you want to keep the parameter file and make a copy,
                use pylenstool.keep_only_some_potentials(parameter_file,pot_to_keep)

                Parameters
                ----------
                pot_to_keep : list of string
                              list of potential names
                """
                a=[]
                for el in self.core:
                        if el[0][0]!='potentiel':
                                a.append(el)
                        if el[0][0]=='potentiel':
                                for pot_name in pot_to_keep:
                                        if el[0][1]==pot_name:
                                                a.append(el)
                self.core=a
        def zoom_window(self,RAcen,DECcen,size,verbose=True):  # Not very pretty function, need to be rewrite
                """
                Change the field (formerly champ) section to zoom on a square around the position
                If RAcen and DECcen equal 0 the reference positions are taken as the center point 
                
                Parameters
                ----------
                RAcen : The right ascension in degrees
                DECcen : The declination in degrees
                size : size of one edge of the square in arcsec
                verbose : Boolean Activate some comments (default is True) 
                """
                x=[el for section in self.core for el in section]
                for val in x:
                        if val[0]=='reference':
                                RAref=float(val[2])
                                DECref=float(val[3])
                if RAcen==0 and DECcen==0:
                        xmin=-1*float(size)/2.0
                        xmax=xmin+float(size)
                        ymin=-1*float(size)/2.0
                        ymax=ymin+float(size)
                else:
                        xmin=-1*(RAcen-RAref)*3600.0*math.cos(DECref*(math.pi/180.0))-float(size)/2.0
                        #VERBOSE
                        #print RAcen,RAref,(RAcen-RAref)*3600.0,(RAcen-RAref)*3600.0*math.cos(DECref*(math.pi/180.0)),xmin
                        xmax=xmin+float(size)
                        ymin=(DECcen-DECref)*3600.0-float(size)/2.0
                        ymax=ymin+float(size)
                champ=False
                field=False
                for sec in self.get_section(): #This is to remain backward compatible, as it is it will keep the old version if it exists
                   if sec[0]=='champ':
                      champ=True
                   else :
                      field=True
                if champ:
                   self.set_parameter("champ","xmin "+str(xmin),verbose=verbose)
                   self.set_parameter("champ","ymin "+str(ymin),verbose=verbose)
                   self.set_parameter("champ","xmax "+str(xmax),verbose=verbose)
                   self.set_parameter("champ","ymax "+str(ymax),verbose=verbose)
                else:
                   self.set_parameter("field","xmin "+str(xmin),verbose=verbose)
                   self.set_parameter("field","ymin "+str(ymin),verbose=verbose)
                   self.set_parameter("field","xmax "+str(xmax),verbose=verbose)
                   self.set_parameter("field","ymax "+str(ymax),verbose=verbose)

        def s_zoom_window(self,RAcen,DECcen,size,verbose=True):  # Not very pretty function, need to be rewrite
                """
                Change the cleanlens section s_xmax,s_xmin,s_ymax,s_ymin to zoom on a square around the position
                If RAcen and DECcen equal 0 the reference positions are taken as the center point 
                
                Parameters
                ----------
                RAcen : The right ascension in degrees
                DECcen : The declination in degrees
                size : size of one edge of the square in arcsec
                verbose : Boolean Activate some comments (default is True) 
                """
                x=[el for section in self.core for el in section]
                for val in x:
                        if val[0]=='reference':
                                RAref=float(val[2])
                                DECref=float(val[3])
                if RAcen==0 and DECcen==0:
                        xmin=-1*float(size)/2.0
                        xmax=xmin+float(size)
                        ymin=-1*float(size)/2.0
                        ymax=ymin+float(size)
                else:
                        xmin=-1*(RAcen-RAref)*3600.0*math.cos(DECref*(math.pi/180.0))-float(size)/2.0
                        #VERBOSE
                        #print RAcen,RAref,(RAcen-RAref)*3600.0,(RAcen-RAref)*3600.0*math.cos(DECref*(math.pi/180.0)),xmin
                        xmax=xmin+float(size)
                        ymin=(DECcen-DECref)*3600.0-float(size)/2.0
                        ymax=ymin+float(size)
                self.set_parameter("cleanlens","s_xmin "+str(xmin),verbose=verbose)
                self.set_parameter("cleanlens","s_ymin "+str(ymin),verbose=verbose)
                self.set_parameter("cleanlens","s_xmax "+str(xmax),verbose=verbose)
                self.set_parameter("cleanlens","s_ymax "+str(ymax),verbose=verbose)

        def set_critic_and_caustic_curve(self,z='2.0',limitHigh='0.2',limitLow='0.1',verbose=True):
           """
           Change the cline section to implement the command leading to production of critical curve
           Note: to plot the lines on ds9 use the lenstool utils pcl
                
           Parameters
           ----------
           z : string or table of string
               redshift or list of redshift
           limitHigh : string
                       value of the largest allowed segment in drawing the critical curve  Check lenstool doc for more details
           limitLow : string
                       value of the smmallest allowed segment in drawing the critical curve Check lenstool doc for more details
           verbose : Boolean 
                     Activate some comments (default is True) 
           """
           #print(type(z),z)
           #print(isinstance(z,str))
           #print(isinstance(z,list))
           if isinstance(z,list) or isinstance(z,tuple):
              nb_plan=str(len(z))
              z_list_str=[' '+zz for zz in z]
              self.set_parameter('cline','nplan '+nb_plan+z_list_str,verbose=verbose)
           elif isinstance(z,str):
              self.set_parameter('cline','nplan 1 '+z,verbose=verbose)
           else:
              print('PYLENSTOOL ERROR: the type of the z variable should be either string or list, or tuple of string',type(z))
               
           self.set_parameter('cline','limitHigh '+limitHigh,verbose=verbose)
           self.set_parameter('cline','limitLow  '+limitLow,verbose=verbose)

        def check_number_of_lens(self,verbose=True):
                """
                Updates nlentille, nlens_opt, and the number in multfile value 
                to reflect the current number of lenses and multiple images
                """

                nlens_opt_value=0
                nlentille_value=0
                nimage_in_img_mul_file=0
                name_img_mul_file=''
                for el in self.core:
                        if el[0][0]=='limit':
                                nlens_opt_value+=1
                        
                        if el[0][0]=='potentiel' or el[0][0]=='potential':
                                nlentille_value+=1                        
                        
                        if el[0][0]=='potfile' or el[0][0]=='potfile0' or el[0][0]=='potfile1' or el[0][0]=='potfile2':
                                for line in el : 
                                        if line[0]=='filein':
                                                try:
                                                        valu=_simplecount(line[2])
                                                        nlentille_value+=valu
                                                except:
                                                        if verbose:
                                                                print(" ")
                                                                print("ERROR Issue with the filein parameter",line)
                                                                print(" ")
                        if el[0][0]=='image':
                                for line in el :
                                        try :
                                                if line[0]=='multfile':
                                                        name_img_mul_file=line[2]
                                                        valu=_simplecount(line[2])
                                                        nimage_in_img_mul_file+=valu #Until the bug is not fixed I put this value to 1 to switch on the mutlfile keyword
                                                        nimage_in_img_mul_file='1'
                                        except :
                                                print('WARNING pylenstool : Something is wrong with the multfile parameter, it should be: multfile integer filename_arcs ') 
                self.set_parameter('grid','nlentille '+str(nlentille_value),verbose=verbose)
                if nlens_opt_value!=0:
                        self.set_parameter('grid','nlens_opt '+str(nlens_opt_value),verbose=verbose)
                if nimage_in_img_mul_file!=0:
                        self.set_parameter('image','multfile '+str(nimage_in_img_mul_file)+' '+name_img_mul_file,verbose=verbose)
                

        #you can run without taking care about remove your first parameter file
        def write_param_file(self,output_filename='pylenstool_model.par',check_count_lens=True,verbose=True):
                """
                Writes the parameter file which is currently loaded

                Parameters
                ----------
                output_filename : string
                                  gives the name of the output file, by default it is pylenstool_model.par
                check_count_lens    : boolean
                                      False if you do not want to change nlentille and nlens_opt
                                      By default the nlentille and nlens_opt will be updated
                """

                if check_count_lens:
                        self.check_number_of_lens(verbose=verbose)

                #If the user manage the name version we didn't need this if otherwise the 'if' here manage itself
                # I will change the name of the old 'best.par' and i put the date at the end of him 
                #date=str(time.strftime("%d-%m-%Y-%H-%M-%S"))
                date=str(time.strftime("%Y%m%d%H%M%S"))
                if self.input_filename==output_filename:
                        inputfileName, inputfileExtension = os.path.splitext(self.input_filename)
                        print(date)
                        try:
                                os.system('cp '+self.input_filename+' '+inputfileName+inputfileExtension+'.'+date)
                                os.system('rm '+self.input_filename)
                        except:
                                print('WARNING: There is an issue when replacing the old file')
                new_prmfile = open(output_filename,'w')
                new_prmfile.write('#pylenstool modification '+date+'\n')
                for line in self.comments:
                        new_prmfile.write(line)
                for mode in self.core:
                        if len(mode)>1:               # This is to properly write fini at the end of the file
                                if len(mode[0])>=2:    # this to properly write the potential XX and allow to write longer than 1 mode
                                        for elmode in mode[0]: 
                                                new_prmfile.write(str(elmode)+'\t')
                                        new_prmfile.write('\n')
                                else:
                                        new_prmfile.write(str(mode[0][0])+'\n')
                                for line in mode[1:]:                     #this writes the core of the each mode
                                        new_prmfile.write('\t')
                                        new_prmfile.write(str(line[0])+'\t')
                                        for el in line[1:]:
                                                new_prmfile.write(str(el)+' ')
                                        new_prmfile.write('\n')
                        else:#Should only be there to write 'fini' at the end of the file
                                new_prmfile.write(str(mode[0]))
                new_prmfile.close()
        
        def launch_model(self,name='pylenstool_model.par',screen=False,OMP_NUM_THREADS='default',check_count_lens=True,launching_name_of_lenstool='lenstool',verbose=True,DEBUG=False):
                """Launches the parameter file
                
                Parameters
                ----------
                name : string
                       Name of the outputfile by DEFAULT it is pylenstool_model.par
                screen : boolean
                         Puts True if you want to screen your lenstool
                OMP_NUM_THREADS : int
                                  Gives the number of threads you want to use, it takes by default your environnment value
                check_count_lens : boolean
                                   False if you do not want to change nlentille and nlens_opt
                                   By default the nlentille and nlens_opt will be updated
                launching_name_of_lenstool : string
                                             the name of the command that launch lenstool
                                             By default it is lenstool.
                                             (e.g. it could be lenstoolv7 or lenstoolv6.5)
                """
                if verbose:
                        print(name+' will be created')
                        print(name+' will be launched in screen mode: '+str(screen))
                        print(name+' using the following number of CPUs: '+str(OMP_NUM_THREADS))
                self.write_param_file(output_filename=name,check_count_lens=check_count_lens,verbose=verbose)
                if OMP_NUM_THREADS!='default':
                        os.environ["OMP_NUM_THREADS"] = str(OMP_NUM_THREADS)
                if screen:
                        os.system('screen -dm '+launching_name_of_lenstool+' '+name+' -n')
                else :
                        if DEBUG:
                           print('PYLENSTOOL DEBUG : '+launching_name_of_lenstool+' '+name+' -n') 
                        os.system(launching_name_of_lenstool+' '+name+' -n')
        
        def set_parameter(self,section,param,add=False,verbose=True):  # need to add a feature that if the section is limit XX and potentiel is XX they hae to go together mean loop for potentiel when it's limit and loop for limit when it's potentiel.
                """
                Replaces (or creates, if it does not exist) the section and the parameter given in arguments. By default the previous entry will be replaced, unless it is specified by the keyword add that it should be added.
                
                Parameters
                ----------
                section : string
                          Name of the section of parameters, such as runmode, potentiel XX, cline ...
                param : string
                        Full sequence of the parameter in one string eg. 'mass 3 1000 0.308 mass_minim.fits'
                add : boolean
                      If True a new line will be added without replacing, False by default.
                
                eg.   pf.set_parameter('runmode','mass 3 1000 0.308 mass_minim.fits')
                """
                need_to_create_section=True
                does_the_section_has_a_name=False
                section_name=None
                pref_loc=1
                try:                      # define if it's a potentiel or limit to handle the naming
                        if section.split()[0]=='potentiel' or section.split()[0]=='potential' or  section.split()[0]=='limit' :
                                does_the_section_has_a_name=True
                                section_name=section.split()[1]
                                section=section.split()[0]    # I redefine sectiona
                        if section.split()[0][0:7]=='potfile' :
                                does_the_section_has_a_name=True
                                #change as of 17 Feb 
                                #section_name=' ' #Here I trick for the potfile to be at -5 position as well
                                section_name=None
                                section=section.split()[0]    
                except:
                        print('WARNING: the section has no name')
                
        
                for i in range(0,len(self.core)):
                        #print self.core[i][0][0]
                        if self.core[i][0][0]==section and section_name==None:       # if the section already exist and has no name
                                #print('PYLENSTOOL DEBUG',i,section_name,self.core[i])
                                need_to_create_section=False
                                if verbose:
                                   print('                  ',self.core[i][0][0])
                                need_to_create_param=True
                                for j in range(0,len(self.core[i])):
                                        #print self.core[i][j][0]        
                                        #print param.split()[0]
                                        if self.core[i][j][0]==param.split()[0] and add==False:  # if I want to replace parameters
                                                if verbose:
                                                   print(' I replace ',self.core[i][j])
                                                self.core[i][j]=param.split()
                                                if verbose:
                                                   print('by',self.core[i][j])
                                                need_to_create_param=False
                                if need_to_create_param :                                # If I need to create a new parameter
                                        self.core[i][-1]=param.split()
                                        self.core[i].append(['end'])
                                        if verbose:
                                           print('I create a new parameter')
                                           print(self.core[i])
                        if self.core[i][0][0]==section and section_name!=None:# if the section exist and has a name # I do a if inside a if to avoid a try cash with errors
                                #print('PYLENSTOOL DEBUG',i,section_name,self.core[i])
                                pref_loc=i+1 # to have the new potential at the end
                                #print('PYLENSTOOL DEBUG',i,section_name,self.core[i])
                                #print('PYLENSTOOL DEBUG',i,section_name,self.core[i][0])
                                if section_name==self.core[i][0][1]:
                                        if verbose:
                                           print(section,section_name,self.core[i][0])
                                        need_to_create_section=False
                                        need_to_create_param=True
                                        for j in range(0,len(self.core[i])):
                                                if self.core[i][j][0]==param.split()[0] and add==False: # if I want to replace parameters
                                                        if verbose:
                                                           print(' I replace ',self.core[i][j])
                                                        self.core[i][j]=param.split()
                                                        if verbose:
                                                           print('by',self.core[i][j])
                                                        need_to_create_param=False
                                        if need_to_create_param :
                                                self.core[i][-1]=param.split()
                                                self.core[i].append(['end'])
                                                if verbose:
                                                   print('I create a new parameter')
                                                   print(self.core[i])
                if need_to_create_section  and section_name==None :              # If I need to create a new section and has no name
                    if section[0:7]=='potfile':
                        if verbose:
                           print(section+' is created')        
                        self.core.insert(-5,[[section],param.split(),['end']])
                    else:
                        if verbose:
                           print(section+' is created')        
                        self.core.insert(1,[[section],param.split(),['end']])
                if need_to_create_section and section_name!=None :# If I need to create a new section which has a name 
                        if verbose:
                           print(section+' '+section_name+' is created')        
                        # If I look for some feature it could be there to replace the 1 and choose the location        
                        self.core.insert(-5,[[section,section_name],param.split(),['end']])
        


def _set_model(parameter_filename):
        """
        Reads and parses the parameter file to feed the object
         
        Parameters
        ----------
        parameter_filename : string        
                             Name of the parameter file
        
        Returns
        ----------
        core,comments : list,list
                        Returns the core and the comments of the lenstool parameter fed
        """
        names=np.array(['runmode','grid','grille','potential','potentiel','limit','potfile','potfile0','potfile1','potfile2','potfile3','dynfile','shapemodel','shapelimit','cline','grande','large','observ','observe','observation','cosmology','cosmologie','cosmolimit','champ','field','cleanlens','image','source',])
        a=[]
        b=[]
        comments=[]
        still_recording=False
                
        with open(parameter_filename, 'r') as f:
                for line in f:
                        try:
                                if line[0]=='#' or line[0]=='':
                                        comments.append(line)
                                elif np.any(line.split()[0]==names):
                                        b.append(line.split())
                                        still_recording=True
                                elif line.split()[0]=='end' and len(b) >0:
                                        #print("DEBUG: upper container",a,len(a),"lower container",b,len(b))
                                        #print("DEBUG: upper container length",len(a),"lower container",b,len(b))
                                        b.append(line.split())
                                        a.append(b)
                                        b=[]
                                        still_recording=False
                                elif line.split()[0]=='fini' or line.split()[0]=='finish':
                                        a.append(line.split())
                                        still_recording=False
                                elif still_recording:
                                        b.append(line.split())

                        except :
                                print(' ',line)
        return a,comments


def check_overwrite(input_name,output_name,verbose=False,remove=False):
        date=str(time.strftime("%Y%m%d%H%M%S"))
        if input_name==output_name:
                inputfileName, inputfileExtension = os.path.splitext(input_name)
                try:
                        new_name=inputfileName+inputfileExtension+'.'+date
                        if verbose :
                                print(' ')
                                print('####### check_overwrite')
                                print('The input and the output names are matching, to keep things I will')
                                print('keep the input file and change is name to',new_name)
                                print('####### end check_overwrite')
                        os.system('cp '+input_name+' '+new_name)
                        if remove:
                                os.system('rm '+input_name)
                except:
                        print('ERROR : There is an issue when replacing the old file')


def random_selection_bayes(input_name='bayes.dat',how_many='default',keep_best=False,output_name='bayes.dat',verbose=False):
        """
        The routine will randomly select a certain amount of line in the bayes.dat        
        
        Parameters
        ----------
        input_name : string
                    The path of the file where the MCMC chain is stored, default: bayes.dat
        how_many   : int
                    Number of line selected in the MCMC chain files (bayes.dat) default is 1/10 of the lines, if the string 'all' if given, all lines remains
        keep_best : bool
                    Will keep the best parameters as the first line of the newly created file - default: False
        output_name : string
                      The path of the file where the MCMC chain is stored, default: bayes.dat  - if input_name and output_name are the same, the changes will happen and the original file will be stored with the date append to the name of the file.
        verbose : bool
                  Activate some print optoins
        """

        if verbose :
                print('####### random_selection_bayes function ')
                print('I load the file named :',input_name,'as it was a bayes.dat from lenstool')
        if _simplecount(input_name)!=1:

          baba=np.loadtxt(input_name)

          if how_many=='default':
                  how_many=len(baba)/10
          elif how_many=='all':
                  how_many=len(baba)
          how_many=int(how_many)
          if how_many>=len(baba):
                print(' ')
                print('WARNING pylenstool : the number of line asked to the function is equal or higher than the number of line in the file')
                print('No further operation will be conduct on the input file :',input_name)
                print(' ')
                return

          #######reading the bayes comments
          comments_bayes=[]
          f=open(input_name,"r")
          for line in f:
                  if line[0]=='#':
                          comments_bayes.append(line)
          f.close()

          check_overwrite(input_name,output_name,verbose=verbose)

          #######writing the comments
          f2=open(output_name,"w") #write the comments
          for line in comments_bayes:
                f2.write(line)
                #string=''
                #for el in line:
                #        string=string+str(el)+' '
                #f2.write(string)
          f2.close()

          #Write the best line if option is activated
          #Take minimum chi2 instead of max Ln, it seems more robust through version and outputs of lenstool
          print('UN keep_best simplecount',keep_best,_simplecount(input_name))
          if keep_best:
             print('keep_best simplecount',keep_best,_simplecount(input_name))
             with open(output_name,'a') as f_best: #write the core
                np.savetxt(f_best,[baba[np.argmin(baba[:,-1])]],fmt="%10.6f")

          #Compute and write the required number of lines in the bayes.dat
          if verbose :
                  print('I am randomly selecting',how_many,'lines in the bayes file')
          if keep_best : #This selection is to have a constant number in the bayes.dat regardless of keeping the best model or not
             new_baba=baba[random.sample(list(range(len(baba))), how_many-1)]
          else:
             new_baba=baba[random.sample(list(range(len(baba))), how_many)]
          if how_many>1 or keep_best==False:
             with open(output_name,'a') as f3: #write the core
                np.savetxt(f3,new_baba,fmt="%10.6f")
          if verbose :
                print('####### end random_selection_bayes')


def wait_until_created(file_path,mute=False,sec=1,sec_limit=43200):
        """
        The routine wait until the file gave in parameter exits        
        
        Parameters
        ----------
        file_path : string
                    The path of the file you want to be created
        mute      : boolean
                    It allows to mute the script
        sec       : int
                    Specify how much time the program have to sleep before checking again is the file exists
        sec_limit : int
                    The limit over the routine will stop if the file still do not exists 
        """        
        tmp_sec=0
        while not os.path.exists(file_path):
                if not mute:
                        print('The program is waiting for ',file_path,' to be created for '+str(tmp_sec)+' seconds    ')
                        
                time.sleep(sec) # It will check every sec second
                
                tmp_sec += sec
                if tmp_sec>sec_limit:
                        print("I stop the program because it run over "+str(sec/3600.0)+" hours")



def remove_potential(pf,pot_to_keep):
        """
        Removes every potential which are in the list of IDs given in argument.
        If you want to keep the original parameter file and make a copy with the updated potentials,
        do not use this method but the function pylenstool.remove_potential(parameter_file,pot_to_keep)
        ---- WARNING ---- this function is not outdated compare to the function of the object pf.remove_potential

        Parameters
        ----------
        pot_to_keep : list of string
                      list of potential names
        """
        a=[]
        for el in pf.core:
                if el[0][0]!='potentiel' or el[0][0]!='potential':
                        a.append(el)
                if el[0][0]=='potentiel' or el[0][0]!='potentiel':
                        to_be_kept=True
                        for pot_name in pot_to_keep:
                                if el[0][1]==pot_name:
                                        to_be_kept=False
                        if to_be_kept:
                                a.append(el)
        pf_new=copy.deepcopy(pf)   #I need to clone the old list, otherwise it has the same number in memory 
        pf_new.core=a
        return pf_new


def keep_potentials(pf,pot_to_keep):  #It is the same function as keep_only_some_potentials
        """
        Removes every potential which is not in the list of IDs given in argument
        and returns a new parameter file.

        Parameters
        ----------
        pf : lenstool_param_file
             pylenstool parameter file
        pot_to_keep : list of string
                      List of potentials names
        
        Returns
        -------
        pf_new : lenstool_param_file
                 Returns a new pylenstool parameter file
        """
        a=[]
        for el in self.core:
	        if el[0][0]!='potentiel' and el[0][0]!='potential':
		        a.append(el)
        if el[0][0]=='potentiel' or el[0][0]=='potential':
                for pot_name in pot_to_keep:
                        if el[0][1]==pot_name:
                                a.append(el)
        pf_new=copy.deepcopy(pf)   #I need to clone the old list, otherwise it has the same number in memory 
        pf_new.core=a
        return pf_new


#The following section will be deleted in the futur
def keep_only_some_potentials(pf,pot_to_keep):
        """
        Removes every potential which is not in the list of IDs given in argument
        and returns a new parameter file.

        Parameters
        ----------
        pf : lenstool_param_file
             pylenstool parameter file
        pot_to_keep : list of string
                      List of potentials names
        
        Returns
        -------
        pf_new : lenstool_param_file
                 Returns a new pylenstool parameter file
        """
        a=[]
        for el in pf.core:
                if el[0][0]!='potentiel':
                        a.append(el)
                if el[0][0]=='potentiel':
                        for pot_name in pot_to_keep:
                                if el[0][1]==pot_name:
                                        a.append(el)
        pf_new=copy.deepcopy(pf)   #I need to clone the old list, otherwise it has the same number in memory 
        pf_new.core=a
        return pf_new



def _simplecount(filename):
        """
        Counts the number of uncommented lines in the file, comment line start with #
        
        Parameters
        ----------
        filename : string
                   file name 
        """
        line_count = 0
        for line in open(filename):
                if line[0]!='#':
                        line_count += 1
        return line_count


def ref3_to_deg_cat(file_name,output_name='outputfile_deg.txt'):
        """
        Transform a catalog in a lenstool format where the position are set in relatic arcseconds
        In a catalog with the same format where the positions are change to absolutde degrees 
        In the lenstool frame it correspond to changing REFERENCE 3 to REFERENCE 0
        
        Parameters
        ----------
        file_name : string     
                    Name of the catalog
        output_name : string  
                      Name of the output file. By default the name will be outputfile_deg.txt
        """
        core=[]
        ##### I READ THE FILE ######
        f=open(file_name, 'r')
        fout=open(output_name, 'w')
        for line in f:
                row=line.split()
                if row[0]=='#REFERENCE':  # I unfortunately remove the working previous if statement 
                        header=row
                        RA_ref=float(header[2])
                        DEC_ref=float(header[3])
                        fout.write('#REFERENCE 0 '+str(RA_ref)+' '+str(DEC_ref)+'\n')
                elif row[0]=='#' and row[1]=='REFERENCE':  # Lenstool do not produce file like that usually
                        header=row
                        print('HERE')
                        RA_ref=float(header[3])
                        DEC_ref=float(header[4])
                        fout.write('#REFERENCE 0 '+str(RA_ref)+' '+str(DEC_ref)+'\n')
                elif line!='':  # No empty line
                        core.append(row)
        try:
                print(header)
        except UnboundLocalError:
                print('EXIT ERROR : I did not find #REFERENCE 3 RA_ref DEC_ref')
                sys.exit(1)
        except:
                print(' ')
                print('something else went wrong with the header, if you need assistance please contact Guillaume Mahler gmahler@umich.edu')
                print(' ')
        f.close()

        ##### I WRITE THE REST of THE FILE ######
        for line in core[1:]:
                end_str=''
                nRA,nDEC=ref3_to_deg(RA_ref,DEC_ref,float(line[1]),float(line[2]))
                for el in line[3:]:
                        end_str=end_str+el+'    '
                end_str=end_str+'\n'
                #fout.write(line[0]+'    '+str(nRA)+'    '+str(nDEC)+'    '+line[3]+'    '+line[4]+'    '+line[5]+'    '+line[6]+'    '+line[7]+'\n')
                fout.write(line[0]+'    '+str(nRA)+'    '+str(nDEC)+'    '+end_str)


        fout.close()
        return 0

def ref3_to_deg(RA_ref,DEC_ref,d_ra,d_dec):
        """
        Transform a realtiv sky position RA and DEC positions in arcsencs in absolute coordinate using a reference position
        
        Parameters
        ----------
        RA_ref : float     
                 Right ascension position of the reference point (in degrees)
        DEC_ref : float     
                  Declinaison position of the reference point (in degrees)
        d_ra : float     
                 Right ascension of the relativ position (in arcsec)
        d_dec : float     
                Declination of the relativ position (in arcsec)
        Returns
        ----------
        RA,DEC : float,float
                 absolute position in RA and DEC (in degrees)
        """
        DEC=DEC_ref+d_dec/3600.0
        RA=RA_ref-d_ra/3600.0/np.cos(DEC_ref/180.0*np.pi)

        return RA,DEC

        #This is ref0 to ref3 too
def deg_to_ref3(RA_ref,DEC_ref,RA,DEC):
        """
        Transform an aboslute sky position RA and DEC positions in degrees in a realtive coordinate using a reference position
        
        Parameters
        ----------
        RA_ref : float     
                 Right ascension position of the reference point (in degrees)
        DEC_ref : float     
                  Declinaison position of the reference point (in degrees)
        RA : float     
                 Right ascension of the object position (in degrees)
        DEC : float     
                Declination of the object position (in  degrees)
        Returns
        ----------
        RA,DEC : float,float
                 relativ poisiotn in RA and DEC (in acresecnonds)
        """
        d_dec=(DEC-DEC_ref)*3600.0
        d_ra=(RA_ref-RA)*np.cos(DEC_ref/180.0*np.pi)*3600.0
        #print RA_ref,DEC_ref,RA,DEC,d_ra,d_dec
        return d_ra,d_dec

def deg_to_ref3_cat(file_name,output_name='outputfile_ref.txt'):
        """
        Transform a catalog in a lenstool format where the position are set in absolute coordinate in degrees
        In a catalog with the same format where the positions are change to be relative coordinate in arcsecs
        In the lenstool frame it correspond to changing REFERENCE 0 to REFERENCE 3
        
        Parameters
        ----------
        file_name : string     
                    Name of the catalog
        output_name : string  
                      Name of the output file. By default the name will be outputfile_deg.txt
        """
        core=[]
        ##### I READ THE FILE ######
        f=open(file_name, 'r')
        fout=open(output_name, 'w')
        for line in f:
                row=line.split()
                if row[0]=='#REFERENCE':  # I unfortunately remove the working previous if statement 
                        header=row
                        RA_ref=float(header[2])
                        DEC_ref=float(header[3])
                        fout.write('#REFERENCE 3 '+str(RA_ref)+' '+str(DEC_ref)+'\n')
                elif row[0]=='#' and row[1]=='REFERENCE':  # Lenstool do not produce file like that usually
                        header=row
                        RA_ref=float(header[3])
                        DEC_ref=float(header[4])
                        fout.write('#REFERENCE 3 '+str(RA_ref)+' '+str(DEC_ref)+'\n')
                elif line!='':  # No empty line
                        core.append(row)
        try:
                print(header)
        except UnboundLocalError:
                print('EXIT ERROR : I did not find #REFERENCE 0 RA_ref DEC_ref')
                sys.exit(1)
        except:
                print(' ')
                print('something else went wrong with the header, if you need assistance please contact Guillaume Mahler gmahler@umich.edu')
                print(' ')
        f.close()

        ##### I WRITE THE REST of THE FILE ######
        for line in core[1:]:
                end_str=''
                nRA,nDEC=deg_to_ref3(RA_ref,DEC_ref,float(line[1]),float(line[2]))
                for el in line[3:]:
                        end_str=end_str+el+'    '
                end_str=end_str+'\n'
                #fout.write(line[0]+'    '+str(nRA)+'    '+str(nDEC)+'    '+line[3]+'    '+line[4]+'    '+line[5]+'    '+line[6]+'    '+line[7]+'\n')
                fout.write(line[0]+'    '+str(nRA)+'    '+str(nDEC)+'    '+end_str)
        fout.close()
        return 0




if __name__== "main__":

        pf=lenstool_param_file()
        

















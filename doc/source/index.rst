.. pylenstool documentation master file, created by
   sphinx-quickstart on Fri Jul 22 08:26:19 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the documentation of PyLenstool!
===========================================

PyLenstool is a python module that can be used to a handle 
`lenstool <https://projets.lam.fr/projects/lenstool/wiki/>`_ parameter files with simple commands.

The idea behind this is to make quick adjustments on lenstool parameter files on the fly and be able to script / 
launch lenstool after.

PyLenstool is only a python wrapper and not a substitute for lenstool.

PyLenstool is a new project then any comments are welcome.

Please contact gmahler@umich.edu for any kind of suggestions and/or questions.

Contents:

.. toctree::
   :maxdepth: 3

   lenstool_param_file
   pylenstool
   getting_started


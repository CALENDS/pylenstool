Installation
------------

You are encouraged to clone the git version of PyLenstool from git-cral by doing ::

 git clone https://git-cral.univ-lyon1.fr/CALENDS/pylenstool.git

Or you can download directely the source code of any version `here <https://git-cral.univ-lyon1.fr/CALENDS/pylenstool/tags>`_ 

You can install PyLenstool as root in the pylenstool directory with this command::

 python setup.py install

Or only for your user account by doing::

 python setup.py install --user

 
Usage
-----

We highlight here an example of usage of pylenstool, which can be found in demo/example_loop_on_potential/

This example loads and modifies an existing model (as a lenstool parameter file) and then launches lenstool from a python script.

More specifically, this example adjusts model parameters to produce simulated observations (through the pixel command of lenstool), keeping only one of the potentials and varying its mass.

The script do_example_of_lensing.py contains the following commands: ::

 import pylenstool as plens
 import numpy as np

 # Loads the lenstool parameter file named 'best_ref.par'
 pf=plens.lenstool_param_file('best_ref.par') 
 
 # Selects in the parameter file only the 
 # potential named 'O1'
 pf.keep_only_some_potentials(['O1'])

 # In the runmode section, sets the 
 # image parameter to 0
 pf.set_parameter('runmode','image 0') 

 # In the runmode section, sets the source 
 # parameter to 1 and sets the name of the 
 # ouput as source.dat
 pf.set_parameter('runmode','source 1 source.dat') 

 # This loop will change the velocity dispersion 
 # of the potential (to increase it mass), 
 # will add a command to create a fits image of a 
 # lensed source and will launch the corresponding model.
 for velo in np.arange(100.0,2000.0,100):  # Loop from 100 km/s to 2000 km/s by step of 100
 
   # In the potentiel O1 change the v_disp parameter to a new value
   pf.set_parameter('potentiel     O1','v_disp     '+str(velo)) 

   # In the runmode section set pixel parameter to produce a lensed images 
   # for each realisation of the model
   pf.set_parameter('runmode','pixel 1 3500 '+str(int(velo))+'_simu.fits')

   # write/launch the corresponding lenstool model
   pf.launch_model()  


You can also launch this example in demo/example_loop_on_potential/ by doing ::

 python do_example_of_lensing.py


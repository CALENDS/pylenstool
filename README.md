# contact email (Guillaume Mahler): gmahler@umich.edu - 

# pylenstool is a python wrapper to handle lenstool parameter files in python
Project of python wrapper for lenstool.
Pylenstool is a library which makes you modifiy and launch a lenstool parameter file as an object.
Pylenstool could be used very usefull for :
- launch multiple models in a loop
- remove (mask) some of the complexity of the lenstool parameter files for people

Pylenstool project is written in python3.X

# Pylenstool contain a suite of scripts
Look under the folder script_pylenstool
For a short description of the available list of scripts look at the file called: list_of_available_scripts.txt 
In any scripts you will find a little descirption inside, just above the ___main___ script.

If you find those instructions too obscured contact directly Guilllaume Mahler, I will be willing to help you.



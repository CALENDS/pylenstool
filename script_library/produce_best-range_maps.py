import pylenstool as plens
import os
from astropy.cosmology import FlatLambdaCDM
import optparse
#from astropy import units as u
import numpy as np
import astropy.io.fits as fits
import time
import sys
import shutil

#import psutil

def define_parser():
        parser = optparse.OptionParser()
        parser.add_option('-p', '--prefix', dest='prefix_map', help='insert a prefix name to each map e.g. hlsp_frontier_model_abell2744_cats_v4_ ')
        parser.add_option('-n', '--nb_pixel', dest='nb_pixel', help='Set the number of pixel you want the reslting image will be int x int pixels ')
        parser.add_option('-f', '--fov', dest='fov', type=float,help='Define the length in arcsec of 1 side of the image will be square fov x fov ')
        parser.add_option('-C', '--center', dest='center', help='define the center, default is the reference value of the lens model')
        parser.add_option('-c', '--how_many_cpu', type=int,dest='how_many_cpu', help='define how many CPU you want to use in parallel')
        parser.add_option('-N', '--Nmax', dest='Nmax', help='Define the maximum number of map that will be produced by  bayesMap note: since the best map is always the first, doing -N 1 will give you the best map')
        parser.add_option('-m', '--map_type', dest='map_type', help='give a string or a list of comma-seprated string to define which map will be produced such as k,g,p,m,srcamp,dxy,g1,g2,mag2,mag9,mag_Z.zz which refers to kappa, gamma, potential, mass, ampli-3 ,deflection maps, gamma1, gamma2, magnification at z=2, magnification at z=9, magnification at the specify redshift Z.zz e.g. mag_2.39 for a map at z=2.39 - maps will be created in the given order')
        parser.add_option( '--bayesMap_loc', dest='bayesMap_loc', help=' the location of the bayesMap use')
        parser.add_option( '-d','--dlsds1', dest='dlsds1', default='True', help='True or False - to normalise the maps to dlsds=1 - p, dxy, g1, g2 will be normalised unless False is specified default is True - if False, specify redshifts in z_input')
        parser.add_option( '-z','--redshift', dest='redshift', default='9.0', help='Only change this options if you want p, dxy, g1, g2 not normalized to dlsds=1 but left to a specific redshift - ONLY WORK if dlsds is set to false - to change magnification map redsfhits use map_type mag_....')
        #parser.add_option('-v', '--version', dest='version', help='define the version of lenstool used to generate the bayes.dat (e.g. 6.5  or 7 )')
        parser.add_option('-v', '--verbose', dest='verbose',default='False',action='store_true', help='verbose - default True')
        return parser

def define_full_parameter(options, args):
        if options.center is None:
            options.center=[0,0]
        else:
            RA=float(options.center.split(',')[0])
            DEC=float(options.center.split(',')[1])
            options.center=[RA,DEC]
        if options.redshift is None:
            options.redshift='9.0'
        if options.bayesMap_loc is None:
                options.bayesMap_loc='bayesMap' 
                #options.bayesMap_loc='/Users/gmahler/soft/lenstool/utils/bayesMap'
                #options.bayesMap_loc='/usr/local/lenstool-v7-git/lenstool/utils/bayesMap' #on cirrus 
        if options.prefix_map is None:
                options.prefix_map=''
        if options.nb_pixel is None:
                options.nb_pixel='2000'
        if options.fov is None:
                options.fov=100
        if options.how_many_cpu is None:
                options.how_many_cpu=1
        if options.Nmax is None:
                options.Nmax='100' #bayesMap do not take more than 2000 line in the bayesMap
        if options.map_type is None:
                options.map_type=['p']
        else:
                options.map_type=options.map_type.split(',')
        if options.dlsds1=='True':
                options.dlsds1=True
        elif options.dlsds1=='False':
                options.dlsds1=False
        else:
            print("PYLENSTOOL ERROR: Something is wrong with the dlsds1 parameter definition definition")
        if options.verbose=='True':
                options.verbose=True
        elif options.verbose=='False':
                options.verbose=False
        else:
            print("PYLENSTOOL ERROR: Something is wrong with the verbose parameter definition definition")
        return options,args



def dic_map_lenstool():
    dic={'k': 'ampli 5','g': 'ampli 6','p':'poten 1','mag9':'ampli 2','mag2':'ampli 2','m':'mass 4','dxy':'dpl 1','g1':'shear 3','g2':'shear 4','srcamp':'ampli -3'}
    return dic

def change_name(what_map,prefix_map):
        print('######')
        print('change name')
        print('######')
        new_tmp='tmp_'+what_map
        os.system('mkdir '+new_tmp)
        os.system('rm '+new_tmp+'/*')
        os.system('mv tmp/* '+new_tmp)
        for map_name in os.listdir(new_tmp):
                file_path=new_tmp+'/'+map_name
                print(new_tmp+'/'+prefix_map)
                print(map_name)
                #new_file_path=new_tmp+'/'+prefix_map.split('_cats_')[0]+'_cats-map'+map_name.split('_')[-1].strip('.fits').zfill(4)+'_'+prefix_map.split('_cats_')[1]+what_map+'.fits'
                if what_map=='dxy':
                        print(map_name.split('_')[1])
                        if map_name.split('_')[1]=='dx':
                                print('                                          DX')
                                new_file_path=new_tmp+'/'+prefix_map+map_name.split('_')[-1].strip('.fits').zfill(4)+'_dx.fits'
                                print(file_path,new_file_path)
                                os.system('mv '+file_path+' '+new_file_path)
                        else:
                                print('                                          DY')
                                new_file_path=new_tmp+'/'+prefix_map+map_name.split('_')[-1].strip('.fits').zfill(4)+'_dy.fits'
                                print(file_path,new_file_path)
                                os.system('mv '+file_path+' '+new_file_path)
                else:
                        new_file_path=new_tmp+'/'+prefix_map+map_name.split('_')[-1].strip('.fits').zfill(4)+'_'+what_map+'.fits'
                        print(file_path,new_file_path)
                        os.system('mv '+file_path+' '+new_file_path)
                #map000_v2_kappa.fits
                #aaa.split('_')[-1].strip('.fits').zfill(4)
        return 0

def launch_bayesMap(pf,prefix_map,nb_pixel,fov,center,how_many_cpu,what_map,Nmax,time_start,bayesMap_loc,redshift,verbose=True):
        dic=dic_map_lenstool()
        
        pf.clean()
        if what_map=='dxy':
                pf.set_parameter('runmode',dic[what_map]+' '+nb_pixel+' '+redshift+' tmp_dx_.fits tmp_dy_.fits',verbose=verbose)
        elif what_map=='m':
                pf.set_parameter('runmode',dic[what_map]+' '+nb_pixel+' '+pf.get_zlens()[0]+' tmp_'+what_map+'_.fits',verbose=verbose)
        elif what_map[0:4]=='mag_':
                zzz=what_map.split('_')[1]
                pf.set_parameter('runmode','ampli 2 '+nb_pixel+' '+zzz+' tmp_'+what_map+'_.fits',verbose=verbose)
        else :
                pf.set_parameter('runmode',dic[what_map]+' '+nb_pixel+' '+redshift+' tmp_'+what_map+'_.fits',verbose=verbose)
        pf.zoom_window(center[0],center[1],fov)
        #ampli 6 '+nb_pixel+' 9.0 tmp_gamma.fits')
        
        name_output='pylenstool_model_'+what_map+'.par'
        pf.write_param_file(output_filename=name_output)
        #time.sleep(0.1)
        if verbose:
                print('I remove everything in the tmp directory')
        os.system('rm tmp/*')

        plens.check_overwrite('bayes.dat','bayes.dat')
        time.sleep(1.0)
        expand_bayes_x() #This might be included in random_selection_bayes or maybe a pylenstool check
        plens.random_selection_bayes(how_many=Nmax,keep_best=True,verbose=verbose)
        os.system("cp bayes.dat trash.txt ; awk  '{ if ( /000000/ ) {gsub(/000000/,'0', $0)} } 1' trash.txt > bayes.dat ")

        if verbose:
                print('I launch ',how_many_cpu,'instance of bayesMap, from '+bayesMap_loc)
        for i in range(0,how_many_cpu):
                os.system(bayesMap_loc+' '+name_output+' &')
                time.sleep(0.1)
        
        aaa=np.loadtxt('bayes.dat')
        last_file=len(aaa)
        if Nmax=='1':
            last_file=1
        if what_map=='dxy':
                last_file=last_file*2
        time.sleep(0.3)
        while not len(os.listdir('tmp/'))>=last_file:
                print('I am waiting to have '+str(last_file)+' file(s) in tmp/ since',time.time()-time_start,'seconds')
                print(os.listdir('tmp/'))
                time.sleep(float(nb_pixel)/300) # It will check every
        
        Glistdir=os.listdir('tmp/')
        print(Glistdir)
        for map_name in Glistdir:
                file_path='tmp/'+map_name
                while not os.path.getsize(file_path)> 10:
                        print('I am here and I am waiting for ',file_path,' to be complete')
                        time.sleep(float(nb_pixel)/300) # It will check every 
        print('every map are produce')
        
        change_name(what_map,prefix_map)
                        
        return 0

#This function expand bayes when in grid mode as there is some compression marked eg. 36x00 meaning need to have 36 times 00
def expand_bayes_x(bayes_file='bayes.dat',bayes_file_bck='bayes_old.dat',verbose=False):

   shutil.copyfile(bayes_file, bayes_file_bck)

   f = open(bayes_file_bck, 'r')
   lines = f.readlines()
   f.close()

   f = open(bayes_file, 'w')

   for line in lines:
        if line[0]=='#':
                f.write(line)
                
                continue
        words = line.split(' ')
        for word in words:
                if 'x' not in word:
                        f.write(word+' ')
                        #if 'n' not in word:
                        #        print("if word",word)
                        #        f.write(word+' ')
                        #else:
                        #        print("else word",word)
                        #        f.write(word)
                else:
                        N = int(word.split('x')[0])
                        f.write('0.0 '*N)
   f.close()
   return 0


def change_bayes_version_update(input_name='bayes.dat',output_name='bayes.dat'):
        bck_name='original_bayes.dat'
        os.system('cp '+input_name+' '+bck_name)
        comments=[]
        f=open(bck_name,"r")
        for line in f:
                if line[0]=='#':
                        comments.append(line)
        f.close()
        comments.append(comments[1])
        comments[1]='#ln(Lhood)\n'

        f=open(output_name,"w")
        data=np.genfromtxt(bck_name)
        data=np.c_[ data[:,0], 2*np.log(data[:,1])+5,data[:,2:],data[:,1] ]
        for line in comments:
                f.write(line)
        np.savetxt(f,data)
        f.close()
        return 0

def write_the_MAPS_README():
        date=str(time.strftime("%Y%m%d%H%M%S"))
        new_prmfile = open('MAPS_README.txt','w')
        new_prmfile.write('#File created date: '+date+'\n')
        new_prmfile.write('#dx.fits        represents the value of the deflection in the x(horizontal) axis normalised to a DLS/DS=1'+'\n')
        new_prmfile.write('#dy.fits        represents the value of the deflection in the y(vertical) axis normalized to a DLS/DS=1'+'\n')
        new_prmfile.write('#g.fits          represents the shear field at a DLS/DS=1'+'\n')
        new_prmfile.write('#k.fits          represents the convergence at a DLS/DS=1'+'\n')
        new_prmfile.write('#m.fits         represents the mass density in  10^12Msol/kpc^2'+'\n')
        new_prmfile.write('#g1.fits      represents amplification matrix factor gamma1 at a DLS/DS=1'+'\n')
        new_prmfile.write('#g2.fits      represents amplification matrix factor gamma2 at a DLS/DS=1'+'\n')
        #new_prmfile.write('#mag2.fits  represents the magnification map at redshifts z=2'+'\n')
        #new_prmfile.write('#mag3.fits  represents the magnification map at redshifts z=3'+'\n')
        #new_prmfile.write('#mag4.fits  represents the magnification map at redshifts z=4'+'\n')
        #new_prmfile.write('#mag5.fits  represents the magnification map at redshifts z=5'+'\n')
        #new_prmfile.write('#mag6.fits  represents the magnification map at redshifts z=6'+'\n')
        #new_prmfile.write('#mag7.fits  represents the magnification map at redshifts z=7'+'\n')
        #new_prmfile.write('#mag8.fits  represents the magnification map at redshifts z=8'+'\n')
        #new_prmfile.write('#mag9.fits  represents the magnification map at redshifts z=9'+'\n')
        new_prmfile.write('#mag_Z.zz.fits  represents the magnification map at redshifts z=Z.zz'+'\n')
        #new_prmfile.write('#mag10.fits represents the magnification map at redshifts z=10'+'\n')
        new_prmfile.write('#p.fits     represents the lensing potential normalised to a DLS/DS=1'+'\n')
        new_prmfile.write('#srcamp.fits     represents the mapping of the  amplification in the source plan combined with sensitivity map '+'\n')
        new_prmfile.close()

def check_update_bayes_version(input_name='bayes.dat',verbose=True):
        """
        This will update the bayes.dat version to be compatible with version 7 and above
        """
        comments=[]
        f=open(input_name,"r")
        version='7'
        for line in f:
                if line[0]=='#':
                        comments.append(line)
        f.close()
        #print('DEBUG:',comments[1],comments[1][0:4])
        if comments[1][0:4]=='#Chi':
            version='6.5'
            change_bayes_version_update()
            if verbose:
               print('You are using the settings made for the version 6.5 of lenstool\nThe bayes.dat has been update')
        else:
            if verbose:
               print('The bayes.dat is compatible with lenstool version 7 and above')
        return version

def print_options(options):
   print(' ')
   print('################################')
   print('Map generated: ',options.map_type)
   if options.dlsds1:
       print('Which will be normalised to dlsds1 (only valid for p,g1,g2,dxy)')
   else:
       print('NOT normalised to dlsds1 (only valid for p,g1,g2,dxy)')
       print('Maps computed for z='+options.redshift)
   print('Number per type of map: '+options.Nmax)
   print('Number of pixel: '+options.nb_pixel)
   print('Field of view (arcsec): ',options.fov)
   print('Resulting resolution arcsec/pixel: ',options.fov/float(options.nb_pixel))
   if options.prefix_map != '':
       print('Every maps will start with the prefix :'+options.prefix_map)
   print('##')
   print('Location of the lenstool utils used: '+options.bayesMap_loc)
   print('CPU launched in parallel: ',options.how_many_cpu)
   print('################################')
   print(' ')
   return 0

def normalise_image(image_file,z_bck,z_lens):
        z_lens=float(z_lens)
        DLSDS=compute_DLSDS(z_lens,z_bck)
        hdu_list = fits.open(image_file)
        hdu_list[0].data=hdu_list[0].data/DLSDS.value
        hdu_list.writeto(image_file, overwrite=True) #BECARFULL IT WILL ERASE THE PREVIOUS FILE
        return 0

def compute_DLSDS(z_lens,z_bck_source):
        cosmo = FlatLambdaCDM(H0=70, Om0=0.3)

        DL=cosmo.angular_diameter_distance(z_lens)
        DS=cosmo.angular_diameter_distance(z_bck_source)

        scl_zS=cosmo.scale_factor(z_bck_source) # 1/ (1+z) scaling factor at z source
        DML=cosmo.comoving_distance(z_lens)  #Comiving distance (tranverse) for zLens 
        DMS=cosmo.comoving_distance(z_bck_source)  #Comiving distance (tranverse) for zS
        DH=cosmo._hubble_distance
        Ok=cosmo.Ok0  # For flat LCDM it s Omegak=0
        DLS=scl_zS*(DMS*np.sqrt(1+Ok*(DML**2)/(DH**2))-DML*np.sqrt(1+Ok*(DMS**2)/(DH**2) ))

        DLSDS=DLS/DS
        return DLSDS


def do_normalisation(z_lens,what_map,z_bck):
    if what_map=='p' or what_map=='dxy' or what_map=='g1' or what_map=='g2':
        Glistdir=os.listdir('tmp_'+what_map+'/')
        for map_name in Glistdir:
                print(map_name)
                normalise_image('tmp_'+what_map+'/'+map_name,float(z_bck),z_lens)

        #Glistdir=os.listdir('tmp_p/')
        #for map_name in Glistdir:
        #        print(map_name)
        #        normalise_image('tmp_p/'+map_name,9.0,z_lens)


def get_pf(name_par):
    pf=plens.lenstool_param_file(name_par)
    pf.clean()
    return pf

def image_names(prefix_map):
        #image_name_gamma1=prefix_map+'gamma1.fits'
        #image_name_gamma2=prefix_map+'gamma2.fits'
        #image_name_xdeflect=prefix_map+'dx.fits'
        #image_name_ydeflect=prefix_map+'dy.fits'
        image_name_poten=prefix_map+'p.fits'
        image_name_to_normalised=[image_name_xdeflect,image_name_ydeflect,image_name_poten]
        #image_name_to_normalised=[image_name_gamma1,image_name_gamma2,image_name_xdeflect,image_name_ydeflect,image_name_poten]

        return image_name_to_normalised

if __name__== "__main__":

        #Parse the option
        parser = define_parser()
        (options, args) = parser.parse_args()
        options,args=define_full_parameter(options, args)
        #
        
        if options.verbose:
           print_options(options)
        
        print(' ')
        print('One need to have bayes.dat arcs.dat input.par and galcat.cat')
        print('In thie running directory with a copy of the script ')
        print(' ')
        print('Feed it like python produce_best-range_maps.py input.par')
        print(' ')
        print('#################################################')
        print(' ')
        
        
        name_par=str(sys.argv[1])
        pf=get_pf(name_par)     
        
        check_update_bayes_version(verbose=options.verbose)
        
        time_start=time.time()
        write_the_MAPS_README()
        os.system('mkdir tmp')

        #for what_map in ['k','g','p','mag9','mag2','m','dxy']:
        for what_map in options.map_type:
                launch_bayesMap(pf,options.prefix_map,options.nb_pixel,options.fov,options.center,
                        options.how_many_cpu,what_map,options.Nmax,time_start,options.bayesMap_loc,options.redshift,verbose=options.verbose)
                if options.dlsds1:
                   do_normalisation(pf.get_zlens()[0],what_map,options.redshift)
        if options.verbose:
           print(' ')
           print('OVER')
           os.system('pwd')




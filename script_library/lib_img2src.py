import pylenstool as plens
import numpy as np
from astropy.io import fits
from astropy import wcs
import os


def write_image_file(file_data,file_name):
                img_file = open(file_name,'w')
                for line in file_data:                #this writes the core of the each mode
                        for el in line:
                                img_file.write(str(el)+' ')
                        img_file.write('\n')
                img_file.close()

##REFERENCE 3 8.4214360 2.7046910
def ds9regpoly2img(z_source,lens_model='best.par',name_reg='ds9.reg',lenstool_name='lenstool'):
                os.system('rm source.dat')  # To be sure that I will not read a unrelated source position guess
                z_source=str(z_source)
                pf=plens.lenstool_param_file(lens_model)
                pf.clean()
                pf.zoom_window(0,0,100)
                ref=pf.get_ref()
                RAref=ref[0]
                DECref=ref[1]
                cosDECref=np.cos(np.pi/180.0*float(DECref))
                name_output_list=[]
                with open(name_reg, 'r') as f:
                        num=0  #Num is the paramter that give name if no name are given to the polygon
                        for line in f:
                                if line[0:7]=='polygon' :
                                        poly=[['#REFERENCE 3 '+RAref+' '+DECref]]
                                        
                                        tmp=line.split('polygon(')[1].split(')')[0]
                                        splitted_line=tmp.split(',')
                                        try : # Does the polygon have a ID   name_text carry the ID of the region 
                                                print(line.split('text={'))
                                                name_text=line.split('text={')[1].split('}')[0]
                                        except:
                                                name_text=''
                                        for i in range(0,int(len(splitted_line)/2)):
                                                dRA=-3600*(float(splitted_line[2*i])-float(RAref))*cosDECref
                                                dDEC=3600*(float(splitted_line[2*i+1])-float(DECref))
                                                poly.append([str(i),str(dRA),str(dDEC),'0.1','0.1','0.0',z_source,'0.0'])

                                        if name_text!='':
                                                name_output='img4srcguess_'+str(name_text)+'.dat'
                                        else :
                                                name_output='img4srcguess_'+str(num)+'.dat'
                                        #name_output_list.append(name_output)
                                        ##### CREATE THE FILE AND LAUNCH PYLENSTOOL AND BCKP THE 
                                        os.system('rm '+name_output)
                                        write_image_file(poly,name_output)   
                                        plens.wait_until_created(name_output)
                                        pf.set_parameter('runmode','image 1 '+name_output)
                                        pf.launch_model(name='pylenstool_model_guess-src_name_text.par',launching_name_of_lenstool=lenstool_name)
                                        plens.wait_until_created('source.dat')
                                        if name_text!='':
                                                src_name_output='srcguess_'+str(name_text)+'.dat'
                                                os.system('mv source.dat '+src_name_output)
                                        else :
                                                src_name_output='srcguess_'+str(num)+'.dat'
                                                os.system('mv source.dat '+src_name_output)
                                        #####
                                        name_output_list.append(src_name_output)
                                        #np.savetxt(name_output,poly,fmt='%i    %2.3f    %2.3f')
                                        num=num+1
                return name_output_list

def best_guess_from_src(na_src,magni_flux,reso,source_oversampling,source_FOV):
    if source_oversampling =='default':
        s_echant=np.round(float(magni_flux))
    else:
        s_echant=float(source_oversampling)
    if source_FOV=='default':    
        aaa=np.loadtxt(na_src)
        size=2*np.round(np.max([np.max(aaa[:,1])-np.min(aaa[:,1]),np.max(aaa[:,2])-np.min(aaa[:,2])]))
    else:
        size=float(source_FOV)
    pix_size=reso/s_echant   #I am not sure that is the most perfect way to do it 
    print('SIZE',size,'PIX_SIZE',pix_size)
    s_n=np.round(size/pix_size)
    return s_n,s_echant        




def source_reconstruction(reg_name,img_name,z_source,lens_model='best.par',prefix_dwcs='poly',center='default',image_oversampling=10,img_pixel_size=0.03,source_oversampling='default',source_FOV='default',verbose=False,lenstool_name='lenstool'):
        z_source=str(z_source)
        pf=plens.lenstool_param_file(lens_model)
        pf.clean()
        
        name_poly=pf.ds9regdeg2dwcs(reg_name,prefix_dwcs=prefix_dwcs,verbose=verbose)  #I create the prefix_dwc_X.dwcs here
        #this is not a good name for the function because it launch lenstool
        name_src=ds9regpoly2img(z_source,lens_model=lens_model,name_reg=reg_name,lenstool_name=lenstool_name) # create the source.dat that will be used for guessing the center
        #The 2 previous output are  list  are the first one able to handle several region ??

        pf.remove_section('cleanlens')
        if verbose: #Not sure this work 
           print("DEBUG The image name is ",img_name)
        pf.set_parameter('cleanlens','cleanset  1 '+z_source)
        pf.set_parameter('cleanlens','ncont 1 clean.fits')
        pf.set_parameter('cleanlens','echant   '+str(image_oversampling))
        pf.set_parameter('cleanlens','imframe 3 '+img_name) #img_name='47-147-5-105_NB_smooth.fits'

        all_im_out=[]
        magnification=image_oversampling #This must be changed at some point
        for nana,na_src in zip(name_poly,name_src):
                s_n,s_echant=best_guess_from_src(na_src,magnification,img_pixel_size,source_oversampling,source_FOV)
                set_the_center(pf,center,nana)
                im_out=nana.split('.dwcs')[0]+'.fits'
                all_im_out.append(im_out)
                print('HEEEEEEEEEEEEEEEEEEEERE',s_n,s_echant)
                
                pf.set_parameter('cleanlens','s_n   '+str(s_n))
                pf.remove_parameter('cleanlens','s_xmin')
                pf.remove_parameter('cleanlens','s_xmax')
                pf.remove_parameter('cleanlens','s_ymin')
                pf.remove_parameter('cleanlens','s_ymax')
                pf.set_parameter('cleanlens','s_echant  '+str(s_echant))
                pf.set_parameter('cleanlens','sframe '+im_out)
                pf.set_parameter('cleanlens','contour 1 '+nana)
                pf.set_parameter('cleanlens','c_image center.dwcs')

                pf.launch_model(name='pylenstool_model_source.par',launching_name_of_lenstool=lenstool_name)
                #pf.launch_model(name='pypy.par')

        return all_im_out

def set_the_center(pf,center,name_contour,verbose=0):

  try:
    if center=='default':
        os.system('cp '+name_contour+' center.dwcs')
    elif center[0]=='ref':
        os.system('echo "1 0.0 0.0" > center.dwcs')
    else:
        RAref,DECref=pf.get_ref()
        RA=center[0]
        DEC=center[1]
        nX,nY=plens.deg_to_ref3(float(RAref),float(DECref),float(RA),float(DEC))
        if verbose >=2:
              print('Center {} {}'.format(nX,nY))
        os.system('echo "1 '+str(nX)+' '+str(nY)+'" > center.dwcs')
  except:
    print('ISSUE with the center')
    

def get_the_pixel_size(image_file_name):
   hdu_list = fits.open(image_file_name)
   #hdu_list.info()
   try:
      pixel_sixe_arcsec=np.round_(np.abs(hdu_list[0].header['CD1_1'])*3600,decimals=3)
   except:
      try:
         pixel_sixe_arcsec=np.round_(np.abs(hdu_list[1].header['CD1_1'])*3600,decimals=3)
      except:
         pixel_sixe_arcsec=input("Enter the pixel size (arcsec): ")
   return pixel_sixe_arcsec  


def fits_to_lenstool_image_file(image_file_name):
   print('image_file_name',image_file_name)
   hdu_list = fits.open(image_file_name)   #open the file
   w = wcs.WCS(hdu_list[0].header)       #get the WCS out of it
   n_x,n_y=np.shape(hdu_list[0].data)    #record the lenght in pixel of the image 
   pixcrd = np.array([[0, 0], [0, n_y-1], [n_x-1, 0],[n_x-1, n_y-1]], np.float_)  # get the array of the corner of the image
   world = w.wcs_pix2world(pixcrd, 1)  #The the corner into RA and DEC
   return world


def write_the_file(tab,output_filename,z_source):
    new_file = open(output_filename,'w')
    for i,line in zip(range(0,len(tab)),tab) :
        string=str(i+1)+' '+str(line[0])+' '+str(line[1])+' 0.1 0.1 0.0 '+str(z_source)+' 0.0'
        new_file.write(string+'\n')
    new_file.close()



#all_im_out = source_reconstruction('symbolic_reg.reg','symbolic_im.fits',args.z_source,lens_model=args.model,prefix_dwcs='reconstruction',image_oversampling=args.samp_im,center=args.center_image,img_pixel_size=pix_size,verbose=verbose,source_oversampling=args.samp_so,source_FOV=args.FOV_so)



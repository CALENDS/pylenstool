import sys
import numpy as np
import pylenstool as plens
import os


#Parse ds8 region file
#The output can handle only circle and ellipse
def parse_ds9(ds9_reg_name):
        res=[]
        for line in open(ds9_reg_name):
                if line[0:6]=='circle':
                        ID=line.split('text={')[1].split('}')[0]
                        tmp=line.split('circle(')[1]
                        RA=tmp.split(',')[0]
                        DEC=tmp.split(',')[1]
                        a=tmp.split(',')[2].split('"')[0]
                        b=a
                        theta='0'
                        z='0.0'
                        mag='0.0'
                        res.append([ID,RA,DEC,a,b,theta,z,mag])
                if line[0:7]=='ellipse':
                        ID=line.split('text={')[1].split('}')[0]
                        tmp=line.split('ellipse(')[1]
                        RA=tmp.split(',')[0]
                        DEC=tmp.split(',')[1]
                        a=tmp.split(',')[2].split('"')[0]
                        b=tmp.split(',')[3].split('"')[0]
                        theta=tmp.split(',')[4].split(')')[0]
                        z='0.0'
                        mag='0.0'
                        res.append([ID,RA,DEC,a,b,theta,z,mag])

        print(res)
        return res

#Save the newly created arcfile
def save_arcsfile(tab,new_name):
        new_arcfile = open(new_name,'w')
        for line in tab:
                for el in line:
                        new_arcfile.write(str(el)+' ')
                new_arcfile.write('\n')

#Turn a ds9region file in a arcfile usable by lenstool
def ds9_to_arcfile(ds9_reg_name):
        arcs=parse_ds9(ds9_reg_name)

        IDs=[]
        for line in arcs:
                IDs.append(int(float(line[0])))
        IDs=np.unique(np.array(IDs)).astype(str).tolist()

        new_name=ds9_reg_name.split('.reg')[0]+'.dat'
        save_arcsfile(arcs,new_name)
        return new_name,IDs


#The function that actually edit the lenstool parameter file
def change_and_launch(pf,IDs,name_arc_candidate):
        pf.set_parameter('runmode','inverse 3 0.5 200')
        pf.set_parameter('image','multfile        1 '+name_arc_candidate)
        pf.set_parameter('image','forme   -2')
        pf.set_parameter('image','mult_wcs        3')

        pf.remove_parameter('image','z_m_limit')

        print('IDs',IDs)
        z_low=float(pf.get_zlens()[-1])+0.2 #I fix a lower value for the redshift to be 0.2 higher than the cluster redshifts
        for ID in IDs: #This loop should take care of all the value 
                print(ID,z_low)
                pf.set_parameter('image','z_m_limit    1  '+str(ID)+'  1 '+str(z_low)+' 10. 0.1',add=True)

        pf.set_parameter('image','sigposArcsec    0.3')
        
        pf.launch_model(name='z_optimization_model.par')

#I  take the paramter file again and make the preidction of the image used
def run_prediction_and_analysis(pf,name_arc_candidate):
        pf.set_parameter("runmode","image 1 "+name_arc_candidate)
        pf.launch_model()
        os.system('bayesChires z_optimization_model.par best')
        os.system("bayesResults.pl")
        os.system("cat chires/chires_best.dat ")
        os.system("pelli image.all magenta ")

if __name__== "__main__":

        print('Feed the code like: python I_optimize_z_using_model.py my_model.par ds9.reg')

        name_best=str(sys.argv[1]) #name of the lenstool parameter model file
        pf=plens.lenstool_param_file(name_best) 

        ds9_reg_name=str(sys.argv[2]) #name of the ds9 region file
        new_arcfile,IDs=ds9_to_arcfile(ds9_reg_name)

        print(' ')
        print(' ')
        change_and_launch(pf,IDs,new_arcfile)

        pf_best=plens.lenstool_param_file('best.par')
        run_prediction_and_analysis(pf_best,new_arcfile)        







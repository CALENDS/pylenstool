import pylenstool as plens
import sys


#The purpose of the program is to check and change the number of nlens_opt, nlentille
#The parameter file will also be formated with a nice indentation.
#
#The program is used as follow:
#       python RnWlenspar.py lenstool_model.par [new_outputname.par]
#If no output is used a copy of previous file will be saved with the date at the end of the file name
#
#If you do a lot of models a lot I recommend having the script as an alias
#Then doing only
#RnWlenspar input.par
#will make the use of this program simple
if __name__== "__main__":

        
        name=str(sys.argv[1])
        try :
            out_name=str(sys.argv[2])
        except:
            out_name=name
        pf=plens.lenstool_param_file(name)
        pf.write_param_file(output_filename=out_name)

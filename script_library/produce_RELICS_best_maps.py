import py3_pylenstool as plens
import os
from astropy.cosmology import FlatLambdaCDM
import optparse
#from astropy import units as u
import numpy as np
import astropy.io.fits as fits
import time


def normalise_image(image_file,z_bck,z_lens):
        z_lens=float(z_lens)
        DLSDS=compute_DLSDS(z_lens,z_bck)
        hdu_list = fits.open(image_file)
        hdu_list[0].data=hdu_list[0].data/DLSDS.value
        hdu_list.writeto(image_file, overwrite=True) #BECARFULL IT WILL ERASE THE PREVIOUS FILE
        return 0

def compute_DLSDS(z_lens,z_bck_source):
        cosmo = FlatLambdaCDM(H0=70, Om0=0.3)

        DL=cosmo.angular_diameter_distance(z_lens)
        DS=cosmo.angular_diameter_distance(z_bck_source)

        scl_zS=cosmo.scale_factor(z_bck_source) # 1/ (1+z) scaling factor at z source
        DML=cosmo.comoving_distance(z_lens)  #Comiving distance (tranverse) for zLens 
        DMS=cosmo.comoving_distance(z_bck_source)  #Comiving distance (tranverse) for zS
        DH=cosmo._hubble_distance
        Ok=cosmo.Ok0  # For flat LCDM it s Omegak=0
        DLS=scl_zS*(DMS*np.sqrt(1+Ok*(DML**2)/(DH**2))-DML*np.sqrt(1+Ok*(DMS**2)/(DH**2) ))
        
        DLSDS=DLS/DS
        return DLSDS

def image_names(prefix_map):
        #image_name_gamma1=prefix_map+'gamma1.fits'
        #image_name_gamma2=prefix_map+'gamma2.fits'
        image_name_xdeflect=prefix_map+'dx.fits'
        image_name_ydeflect=prefix_map+'dy.fits'
        image_name_poten=prefix_map+'p.fits'
        image_name_to_normalised=[image_name_xdeflect,image_name_ydeflect,image_name_poten]
        #image_name_to_normalised=[image_name_gamma1,image_name_gamma2,image_name_xdeflect,image_name_ydeflect,image_name_poten]
        
        return image_name_to_normalised

def define_parser():
        parser = optparse.OptionParser()
        parser.add_option('-p', '--prefix', dest='prefix', help='insert a prefix name to each map e.g. relics_model_elgordo_ ')
        parser.add_option('-m', '--model', dest='model', help='name of the model used to produce the map by default it is the best.par ')
        parser.add_option('-r', '--resolution', dest='resolution', help='set the resolution of the map ')
        parser.add_option('-n', '--pixel_number', dest='pixel_number', help='set the number of pixel ')
        parser.add_option('-f', '--field_of_view', dest='fov', help='length of the field of view in arcsec - square FoV ')
        parser.add_option('-c', '--center', dest='center', help='fix the RA,DEC center - format: RA(degree),DEC(degree) ')
        return parser

def define_full_parameter(options, args):
        if options.prefix is None:
                options.prefix=''
        if options.pixel_number is None:
 
           if options.resolution is None:
                options.pixel_number='1000'
           else:
                if options.fov is None:
                      options.pixel_number=str(200.0/float(options.resolution))
                else:
                      options.pixel_number=str(float(options.fov)/float(options.resolution))
        if options.fov is None:
                options.fov='200'
        if options.model is None:
                options.model='best.par'
        if options.center is None:
                options.center=[0,0]
        else:
                RA=float(args.split(',')[0])
                DEC=float(args.split(',')[1])
                options.center=[RA,DEC]
        return options,args

if __name__== "__main__":
        time_start=time.time()

        #Parse the option
        parser = define_parser()
        (options, args) = parser.parse_args()
        options,args=define_full_parameter(options, args)
        #

        #prefix_map='hlsp_frontier_model_abell2744_cats_v4_'
        prefix_map=options.prefix
        model_name=options.model
        
        try:
                pf=plens.lenstool_param_file(model_name)
        except:
                print('No '+model_name+' was found in this directory, please copy the desired best.par in this directory')
        
        z_lens=pf.get_zlens()[0]   # Look for the z of the lens in the file
        pf.clean()
        pf.zoom_window(options.center[0],options.center[1],options.fov)

        image_name_to_normalised=image_names(prefix_map)
        
        for file_path in image_name_to_normalised:  #to not normalise an already existing map
                os.system('rm '+file_path)

        #pf.set_parameter('runmode','shear 3 '+options.pixel_number+' 9.0 '+image_name_to_normalised[0]) #gamma1
        #pf.launch_model(screen=True,OMP_NUM_THREADS='2')
        #time.sleep(0.1)
        #pf.set_parameter('runmode','shear 4 '+options.pixel_number+' 9.0 '+image_name_to_normalised[1]) #gamma2
        #pf.launch_model(screen=True,OMP_NUM_THREADS='2')
        #time.sleep(0.1)
        #'''        
        #RESET shear 
        #pf.clean()
        
        
        pf.set_parameter('runmode','dpl 1 '+options.pixel_number+' 9.0 '+image_name_to_normalised[0]+' '+image_name_to_normalised[1]) #dfl_x dfl_y
        #pf.launch_model(screen=True,OMP_NUM_THREADS='2',name='pylenstool_dxdy.par')
        pf.launch_model(screen=True,OMP_NUM_THREADS='2',name='pylenstool_model_dxy.par')
        time.sleep(0.1)
        #RESET dpl
        pf.clean()
        
        pf.set_parameter('runmode','poten 1 '+options.pixel_number+' 9.0 '+image_name_to_normalised[2])   #potential
        pf.launch_model(screen=True,OMP_NUM_THREADS='2',name='pylenstool_model_p.par')
        time.sleep(0.1)
        #RESET poten
        pf.clean()

        pf.set_parameter('runmode','mass 4 '+options.pixel_number+' '+z_lens+' '+prefix_map+'m.fits')
        pf.launch_model(screen=True,OMP_NUM_THREADS='2',name='pylenstool_model_m.par')
        time.sleep(0.1)
        #RESET mass
        pf.clean()

        #####Next images do not need to be normalised        
        #pf.set_parameter('runmode','ampli 3 '+options.pixel_number+' 1.0 '+prefix_map+'z01-magnif.fits')
        #pf.launch_model(screen=True,OMP_NUM_THREADS='2')
        #time.sleep(0.1)
        pf.set_parameter('runmode','ampli 3 '+options.pixel_number+' 2.0 '+prefix_map+'mag2.fits')
        pf.launch_model(screen=True,OMP_NUM_THREADS='2',name='pylenstool_model_mag2.par')
        time.sleep(0.1)
        #pf.set_parameter('runmode','ampli 3 '+reso+' 4.0 '+prefix_map+'z04-magnif.fits')
        #pf.launch_model(screen=True,OMP_NUM_THREADS='2')
        #time.sleep(0.1)
        pf.set_parameter('runmode','ampli 3 '+options.pixel_number+' 9.0 '+prefix_map+'mag9.fits')
        pf.launch_model(screen=True,OMP_NUM_THREADS='2',name='pylenstool_model_mag9.par')
        time.sleep(0.1)
        pf.set_parameter('runmode','ampli 5 '+options.pixel_number+' 9.0 '+prefix_map+'k.fits')  #kappa
        pf.launch_model(screen=True,OMP_NUM_THREADS='2',name='pylenstool_model_k.par')
        time.sleep(0.1)
        pf.set_parameter('runmode','ampli 6 '+options.pixel_number+' 9.0 '+prefix_map+'g.fits') #gamma
        pf.launch_model(screen=True,OMP_NUM_THREADS='2',name='pylenstool_model_g.par')
        time.sleep(0.1)
        #'''

        
        for file_path in image_name_to_normalised:
                print('I am waiting to normalised '+file_path)
                while not os.path.exists(file_path):
                        print(file_path)
                        print('I am still waiting for map to be created',time.time()-time_start,' seconds')
                        time.sleep(5) # It will check every 5 second

                if os.path.isfile(file_path):
                        normalise_image(file_path,9.0,z_lens)
                else:
                        raise ValueError("%s isn't a good fits file" % file_path)






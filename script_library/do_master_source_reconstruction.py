import pylenstool as plens
import argparse
import sys
import numpy as np
from astropy.io import fits

from astropy import wcs

import tabCompleter
import readline

import os, errno

import lib_img2src
import lib_src2img
import lib_shapemodel


def write_image_file(file_data,file_name):
                img_file = open(file_name,'w')
                for line in file_data:                #this writes the core of the each mode
                        for el in line:
                                img_file.write(str(el)+' ')
                        img_file.write('\n')
                img_file.close()


def force_symlink(file1, file2):
    try:
        os.symlink(file1, file2)
    except OSError as e:
        if e.errno == errno.EEXIST:
            os.remove(file2)
            os.symlink(file1, file2)


def get_the_pixel_size(image_file_name,step):
   hdu_list = fits.open(image_file_name)
   #hdu_list.info()
   if step!=2:
      try:
         pixel_sixe_arcsec=np.round_(np.abs(hdu_list[0].header['CD1_1'])*3600,decimals=3)
      except:
         try:
            pixel_sixe_arcsec=np.round_(np.abs(hdu_list[1].header['CD1_1'])*3600,decimals=3)
         except:
            pixel_sixe_arcsec=input("Enter the pixel size (arcsec): ")
   if step==2:
      pixel_sixe_arcsec=input("Enter the pixel size (arcsec): ")
   return float(pixel_sixe_arcsec) 


#def define_parser():
def define_args_from_parser():
        #parser = optparse.OptionParser()
        parser = argparse.ArgumentParser()
        parser.add_argument("-v", "--verbosity", action="count", default=0,help="increase output verbosity")
        parser.add_argument('-i', '--image', dest='image', help='Input image')
        parser.add_argument('-r', '--region', dest='region', help='Input ds9 region file made out of polygon(s)')
        parser.add_argument('-z', '--redshift', dest='z_source', help='Redshift of the source plan')
        parser.add_argument('-m', '--model', dest='model' , help='lenstool lens model - default: best.par')
        parser.add_argument('-s', '--sampling_image', dest='samp_im', help='lenstool lens model - direclty code for the echant value parameter of the cleanlens section')
        parser.add_argument('-f', '--FOV_image', dest='FOV_im', default='default',help='size in arcsec of the image plane FoV - by defalut it will be computed to contain the full image')
        parser.add_argument('-S', '--sampling_source', dest='samp_so', default='default' , help='oversampling of the source plan - code for the s_echant value parameter of the cleanlens section - default value is the manification net yet implemented')
        parser.add_argument('-F', '--FOV_source', dest='FOV_so',default='default', help='size in arcsec of the source plane FoV - by defalut it will be computed to contain the contour used')
        #parser.add_argument('--center_source', dest='center_source', nargs='*' ,default='default', help='center of the source plane image - format: RA,DEC  in degrees - default value is computed as the mean position of the countour for each region - special keyword "ref" to get the reference model value')
        parser.add_argument('-c','--center_image', dest='center_image', default='default', nargs='*' ,help='center in the image plane reconstruction for the imgplan to srcplan - format: RA,DEC  in degrees - default value is computed as the mean position of the countour for each region - special keyword "ref" to get the reference model value')
        parser.add_argument( '--resolution_image', dest='reso_im',default='0.2', help='lenstool lens model - direclty code for the echant value parameter of the cleanlens section - this prameter is not implement ')
        parser.add_argument( '--step', dest='step',default=0,type=int ,help='select the step of the process used: step 0 (default) -> send the image in the source plane and lens it back to the image plane, this correspond to step 1 and 2 combined && step 1 -> send the image to the source plan && step 2 -> send a source plan image in the image plan && step 3 ->  do a shape model measurement ')
        parser.add_argument( '--seeing', dest='seeing',default='default',help='value of the seeing - needed for MUSE data ')
        parser.add_argument( '--psf', dest='psf_name',default='default',help='the name of the psf fits file - namely a star profile ')
        parser.add_argument( '--lenstool_name', dest='lenstool_name',default='lenstool',help='the name used in command line to launch lenstool (e.g. lenstoolv7) ')
        
        args = parser.parse_args()

        #The following 4 lines are for the autocompletion of the command
        t = tabCompleter.tabCompleter()
        readline.set_completer_delims('\t')
        readline.parse_and_bind("tab: complete")
        readline.set_completer(t.pathCompleter)
        #

        if args.image is None:
                args.image=input("Enter image name: ")
        if args.region is None :
            if args.step!=2:
                args.region=input("Enter ds9 region name: ")
        if args.z_source is None:
                args.z_source=input("Enter the redshifts of the source: ")
        if args.model is None:
           if os.path.isfile("best.par"):
                args.model='best.par'
           else:
                args.model=input("Enter the name of the lenstool parameter file: ")
        
        #to be changed and adapt in the code
        if args.samp_im is None:
                args.samp_im=40

        
        return args


if __name__== "__main__":
        
        args = define_args_from_parser()

        verbose=False #this verbose option is to be given to pylenstool
        
        pix_size=get_the_pixel_size(args.image,args.step) #This function will ask the user if it cannot find the size of pixel by itself
        if args.verbosity >= 2:
            if args.step==2:
               print("Desired pixel size output is pixel size output is {} ".format(pix_size))
            else:
               print("Pixel size of input image '{}' is {} ".format(args.image,pix_size))
            print("Image oversampling: {} ".format(args.samp_im))
            print("Ds9 region file:  {} ".format(args.region))
            print("Source redshift: {} ".format(args.z_source))
            print("Lens model used: {} ".format(args.model))
            print("The center of the image plane is set to: {} ".format(args.center_image))
            print("The used step is: {} ".format(args.step))
            print("The seeing used is: {}".format(args.seeing))
            print("Lenstool command line used :{}".format(args.lenstool_name))
            verbose=True
        elif args.verbosity >= 1:
            print("Pixel size: {} ".format(pix_size))
            print("Image oversampling: {} ".format(args.samp_im))

        
        if args.step==0:
           os.system('mv bayes.dat bck_bayes.dat')
           force_symlink(args.image,'symbolic_im.fits')
           force_symlink(args.region,'symbolic_reg.reg')
        
           plens.wait_until_created('symbolic_im.fits')
           plens.wait_until_created('symbolic_reg.reg')
      
           all_im_out = lib_img2src.source_reconstruction('symbolic_reg.reg','symbolic_im.fits',args.z_source,lens_model=args.model,prefix_dwcs='reconstruction',image_oversampling=args.samp_im,center=args.center_image,img_pixel_size=pix_size,verbose=verbose,source_oversampling=args.samp_so,source_FOV=args.FOV_so,lenstool_name=args.lenstool_name)
        
           for name_to_be_lensed in all_im_out : 
              #lense_source_plane_image(name_to_be_lensed,args.z_source,output_name='lensed_'+name_to_be_lensed,obs_reso=obs_reso)
              lib_src2img.lense_source_plane_image(name_to_be_lensed,args.z_source,lens_model=args.model,output_name='lensed_'+name_to_be_lensed,img_pixel_size=pix_size,center=args.center_image,FOV_im=args.FOV_im,verbose=verbose,lenstool_name=args.lenstool_name)

        
        if args.step==1:
           os.system('mv bayes.dat bck_bayes.dat')
           force_symlink(args.image,'symbolic_im.fits')
           force_symlink(args.region,'symbolic_reg.reg')

           plens.wait_until_created('symbolic_im.fits')
           plens.wait_until_created('symbolic_reg.reg')

           all_im_out = lib_img2src.source_reconstruction('symbolic_reg.reg','symbolic_im.fits',args.z_source,lens_model=args.model,prefix_dwcs='reconstruction',image_oversampling=args.samp_im,center=args.center_image,img_pixel_size=pix_size,verbose=verbose,source_oversampling=args.samp_so,source_FOV=args.FOV_so,lenstool_name=args.lenstool_name)

        

        if args.step==2:
           os.system('mv bayes.dat bck_bayes.dat')
           force_symlink(args.image,'symbolic_im.fits')
           plens.wait_until_created('symbolic_im.fits')
           #I did not fix it yet but step 2 will not work because name_to_be_lensed is not defined  
           name_to_be_lensed=args.image.split('/')[-1]
           lib_src2img.lense_source_plane_image('symbolic_im.fits',args.z_source,lens_model=args.model,output_name='lensed_'+name_to_be_lensed,img_pixel_size=pix_size,center=args.center_image,FOV_im=args.FOV_im,verbose=verbose,lenstool_name=args.lenstool_name)




        if args.step==3:
           lib_shapemodel.optimize_source_shape(args.image,args.region,args.model,args.z_source,img_pixel_size=pix_size,center=args.center_image,FOV_im=args.FOV_im,seeing=args.seeing,psf_name=args.psf_name,lenstool_name=args.lenstool_name,verbose=args.verbosity)




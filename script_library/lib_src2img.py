import pylenstool as plens
import argparse
import sys
import numpy as np
from astropy.io import fits

from astropy import wcs

import tabCompleter
import readline

import os, errno


def write_image_file(file_data,file_name):
                img_file = open(file_name,'w')
                for line in file_data:                #this writes the core of the each mode
                        for el in line:
                                img_file.write(str(el)+' ')
                        img_file.write('\n')
                img_file.close()


def get_the_pixel_size(image_file_name):
   hdu_list = fits.open(image_file_name)
   #hdu_list.info()
   try:
      pixel_sixe_arcsec=np.round_(np.abs(hdu_list[0].header['CD1_1'])*3600,decimals=3)
   except:
      try:
         pixel_sixe_arcsec=np.round_(np.abs(hdu_list[1].header['CD1_1'])*3600,decimals=3)
      except:
         pixel_sixe_arcsec=input("Enter the pixel size (arcsec): ")
   return pixel_sixe_arcsec  




def fits_to_lenstool_image_file(image_file_name):
   print('image_file_name',image_file_name)
   hdu_list = fits.open(image_file_name)   #open the file
   w = wcs.WCS(hdu_list[0].header)       #get the WCS out of it
   n_x,n_y=np.shape(hdu_list[0].data)    #record the lenght in pixel of the image 
   pixcrd = np.array([[0, 0], [0, n_y-1], [n_x-1, 0],[n_x-1, n_y-1]], np.float_)  # get the array of the corner of the image
   world = w.wcs_pix2world(pixcrd, 1)  #The the corner into RA and DEC
   return world


def write_the_file(tab,output_filename,z_source):
    new_file = open(output_filename,'w')
    for i,line in zip(range(0,len(tab)),tab) :
        string=str(i+1)+' '+str(line[0])+' '+str(line[1])+' 0.1 0.1 0.0 '+str(z_source)+' 0.0'
        new_file.write(string+'\n')
    new_file.close()



#I use this function to lens back the source
#def get_image_lensed(lens_model_name,source_plane_image_name,z_source,output_name='lensed_img.fits',resolution='1000'):
def best_guess_from_img_rec(pf,source_name,img_pixel_size,center,FOV_im,z_source,verbose=0,lenstool_name='lenstool'):  #reso is the resolution of the image you want to compare with, its gonna be twice more resolution

     if FOV_im=='default' or center=='default':
        tab_coord=fits_to_lenstool_image_file(source_name)
        name_tmp_img_guess='tmp_img_guess.dat'
        write_the_file(tab_coord,name_tmp_img_guess,z_source)  #Is gonna create the file that contain the corner of the image then it can be lensed
        os.system('rm image.all')
        pf.clean()
        pf.set_parameter('runmode','source 1 '+name_tmp_img_guess)
        pf.zoom_window(0,0,200)
        pf.launch_model(name='pylenstool_guess_image_plan_size.par',launching_name_of_lenstool=lenstool_name)
        plens.wait_until_created('image.all')        
        aaa=np.loadtxt('image.all')
     if FOV_im!='default':
        FOV_im=int(FOV_im)
     else:
        FOV_im=np.round(1.2*np.max([np.max(aaa[:,1])-np.min(aaa[:,1]),np.max(aaa[:,2])-np.min(aaa[:,2])]))

     #print('CENTERRRRRR',center)
     if center=='default':
        RA=np.mean(aaa[:,1]) #There are in arcsec ref3 mode
        DEC=np.mean(aaa[:,2]) #There are in arcsec ref3 mode
        RAref,DECref=pf.get_ref()
        RA,DEC=plens.ref3_to_deg(float(RAref),float(DECref),RA,DEC) #to put them in DEG
     elif center[0]=='ref':
        RA,DEC=pf.get_ref()
        RA=float(RA)
        DEC=float(DEC)
     else:
        RA=float(center[0]) 
        DEC=float(center[1]) 
     n_pix=int(FOV_im/img_pixel_size)
     if verbose>=2:
        print('The size of the image frame is {}'.format(str(FOV_im)))
        print('On a {} by {} pixel grid'.format(str(n_pix),str(n_pix)))
        print('The resolution will be {}'.format(str(img_pixel_size)))
     return RA,DEC,FOV_im,n_pix

#def lense_source_plane_image(image_name,z_source,lens_model='best.par',output_name='lensed_img.fits',resolution='5000'):
def lense_source_plane_image(image_name,z_source,lens_model='best.par',output_name='lensed_img.fits',img_pixel_size=0.03,center='default',FOV_im='default',verbose=0,lenstool_name='lenstool'):
        #resolution=str(resolution)
        z_source=str(z_source)

        pf=plens.lenstool_param_file(lens_model)
        pf.clean()
        pf.remove_section('cleanlens')
        nRA,nDEC,dxy,n_pix=best_guess_from_img_rec(pf,image_name,img_pixel_size,center,FOV_im,z_source,verbose=verbose,lenstool_name=lenstool_name)
        
                

        pf.clean()
        pf.zoom_window(nRA,nDEC,dxy)
        pf.set_parameter('cleanlens','cleanset  1 '+z_source)
        pf.set_parameter('cleanlens','sframe '+image_name)
        pf.set_parameter('runmode','pixel 2 '+str(n_pix)+' '+output_name)
        pf.launch_model(name='pylenstool_model_projection.par',launching_name_of_lenstool=lenstool_name)

        return 0


if __name__== "__main__":
        
        lense_source_plane_image(name_to_be_lensed,args.z_source,output_name='lensed_'+name_to_be_lensed,img_pixel_size=pix_size,center=args.center_image,FOV_im=args.FOV_im,verbose=verbose)

        














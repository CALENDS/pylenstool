import pylenstool as plens
import argparse
import sys

from astropy import wcs

import tabCompleter
import readline

import os, errno


from mpdaf.obj import Image
import numpy as np
import os
from astropy.io import fits
import sys

import math

def nanmad(arr,msk):
    """ Median Absolute Deviation: a "Robust" version of standard deviation.
        Indices variabililty of the sample.
        https://en.wikipedia.org/wiki/Median_absolute_deviation 
    """
    #arr = np.ma.array(arr).compressed() # should be faster to not use masked arrays.
    arr = np.ma.masked_array(arr,mask=msk).compressed() # should be faster to not use masked arrays.
    arr = arr[np.isfinite(arr)]
    med = np.median(arr)

    return np.median(np.abs(arr - med))

def get_first_sigma(im):
        return 1.4855*nanmad(im.data.data,im.data.mask)

def get_inverse_variance(im):
        return 1.0/((get_first_sigma(im))**2)

def build_im_for_var_computation(im,poly_name):
        exist,poly=ds9reg_to_poly_for_var(poly_name,reverse=True)
        if exist:
           im_for_var=im.copy()
           im_for_var.mask_polygon(poly,inside=False)  # create the mask of the polygon in the mask of the array (not the data)
           im_for_var.crop()
           return im_for_var
        else:
           return im

def ds9reg_to_poly_for_var(poly_name,reverse=False):
        f=open(poly_name,'r')
        exist=False
        DECRA=[]
        RADEC=[]
        for line in f:
                if line[0:7]=='polygon' and line[-5:-2]=='var': #The end part with test on var is very specific
                        exist=True
                        lili=line.strip('polygon(')
                        lili=lili.split(')')[0]
                        lili=lili.split(',')
                        if reverse:
                                for i in range(0,int(len(lili)/2)):
                                        DECRA.append([float(lili[2*i+1]),float(lili[2*i])])
                        else :
                                for i in range(0,int(len(lili)/2)):
                                        RADEC.append([float(lili[2*i]),float(lili[2*i+1])])

        f.close()
        if reverse :
                return exist,DECRA
        else :
                return exist,RADEC

def ds9reg_to_poly_for_crop(poly_name,reverse=False):
        f=open(poly_name,'r')

        for line in f:
                if line[0:7]=='polygon' and line[-5:-2]!='var': #The end part with test on var is very specific
                        lili=line.strip('polygon(')
                        lili=lili.split(')')[0]
                        lili=lili.split(',')
                        if reverse:
                                DECRA=[]
                                for i in range(0,int(len(lili)/2)):
                                        DECRA.append([float(lili[2*i+1]),float(lili[2*i])])
                        else :
                                RADEC=[]
                                for i in range(0,int(len(lili)/2)):
                                        RADEC.append([float(lili[2*i]),float(lili[2*i+1])])

        f.close()
        if reverse :
                return DECRA
        else :
                return RADEC

def get_name_from_poly(poly_name):
        prefix_name='noname'
        f=open(poly_name,'r')
        for line in f:
           try:
                if line[0:7]=='polygon' and line[-5:-2]!='var':
                   lili=line.split('text={')[1]
                   prefix_name=lili.split('}')[0]
           except IndexError:
                os.system('cat '+poly_name) 
                print('Something is wrong the region file, maybe the polygon has no name')
                print(stop)
        return prefix_name

#The astropy trick to make image ''good'' for lenstool 
def save_fits_from_mpdaf_to_be_good_for_lenstool(image_mpdaf,name_file,delete_tmp=False):
        image_mpdaf.var=None
        image_mpdaf.write('astropy-trick-tmp.fits',savemask=None)

        hdulist = fits.open('astropy-trick-tmp.fits')
        hdu_small=hdulist[1]
        fits.writeto(name_file,hdu_small.data,header=hdu_small.header,overwrite=True)
        if delete_tmp:
           os.system('rm astropy-trick-tmp.fits')
        return 0

def save_mask_name(name_im):
        f=open('name_mask.txt','w')
        cwd = os.getcwd()
        #print(cwd)
        for line in name_im:
                f.write(cwd+'/'+line+'\n')
        f.close()
        return 0


def cut_and_mask(im_name,poly_name,verbose=0):

        try:
           im=Image(im_name)
        except:
           im=Image(im_name,ext=0)
           if verbose>=1:
              print('        script-WARNING the extension 0 was used for the image')

        prefix_name=get_name_from_poly(poly_name) #I change the name of the file because lenstool cennot handle too long names

        im_for_var_computation=build_im_for_var_computation(im,poly_name)
        inv_var=get_inverse_variance(im_for_var_computation) 
        if verbose>=2:
           print('Total image 1/variance:',inv_var)

        poly=ds9reg_to_poly_for_crop(poly_name,reverse=True) #convert region file to a polygon at the right format
        im.mask_polygon(poly,inside=False)  # create the mask of the polygon in the mask of the array (not the data)
        im.crop()

        #im_name_cropped=im_name.split('/')[-1].split('.fits')[0]+'_cropped.fits'  
        im_name_cropped=prefix_name+'_cropped.fits'  
        save_fits_from_mpdaf_to_be_good_for_lenstool(im,im_name_cropped,delete_tmp=True)
        if verbose>=2:
           print('The image is croppped - name:',im_name_cropped)

        im._data[:,:]=0.0 #data are put to 0 
        im._data[np.where(im.data.mask==False)]=inv_var #replace to value in the data table by the desired number

        #im_name_mask=im_name.split('/')[-1].split('.fits')[0]+'_mask.fits'
        im_name_mask=prefix_name+'_mask.fits'
        save_fits_from_mpdaf_to_be_good_for_lenstool(im,im_name_mask,delete_tmp=True)
        if verbose>=2:
           print('Mask created - name:',im_name_mask)

        return im_name_cropped,im_name_mask

def write_image_file(file_data,file_name):
                img_file = open(file_name,'w')
                for line in file_data:                #this writes the core of the each mode
                        for el in line:
                                img_file.write(str(el)+' ')
                        img_file.write('\n')
                img_file.close()


def compute_source_position(lens_model_name,region_name,z_source,lenstool_name,verbose):
        RADEC=get_circle_RA_DEC(region_name)
        
        src_pos_filename=launch_model_to_get_src_position(lens_model_name,RADEC,z_source,lenstool_name,verbose)
        dRAdDEC=grad_the_value(src_pos_filename)
        return dRAdDEC

def grad_the_value(src_pos_filename):
        aaa=np.loadtxt(src_pos_filename) #only work if there is 1 object in the file  
        
        if aaa.ndim>1: 
           dRAdDEC=[]
           for line in aaa:
              dRA=line[1]
              dDEC=line[2]
              dRAdDEC.append([dRA,dDEC])
        else:
           dRAdDEC=[[aaa[1],aaa[2]]]
        return dRAdDEC

def launch_model_to_get_src_position(lens_model_name,RADEC,z_source,lenstool_name,verbose):
        #num=0  #Num is the paramter that give name if no name are given to the polygon
        if verbose>=2:
           ver_pylenstool=True
        else:
           ver_pylenstool=False
        pf=plens.lenstool_param_file(lens_model_name)
        pf.clean()
        pf.zoom_window(0,0,100,verbose=False)
        ref=pf.get_ref()
        RAref=ref[0]
        DECref=ref[1]
        poly=[['#REFERENCE 3 '+RAref+' '+DECref]]
       
        for i,line in zip(range(0,len(RADEC)),RADEC):
           RA=line[0]
           DEC=line[1] 
           cosDECref=np.cos(np.pi/180.0*float(DECref))
           dRA=-3600*(float(RA)-float(RAref))*cosDECref
           dDEC=3600*(float(DEC)-float(DECref))
        
           poly.append([str(i),str(dRA),str(dDEC),'0.1','0.1','0.0',z_source,'0.0'])
        
        name_output='img4srcguess.dat'
        os.system('rm '+name_output)
        write_image_file(poly,name_output)
        plens.wait_until_created(name_output,mute=True)
        pf.set_parameter('runmode','image 1 '+name_output,verbose=False)
        os.system('rm source.dat')
        pf.launch_model(name='pylenstool_model_guess-src.par',verbose=ver_pylenstool,launching_name_of_lenstool=lenstool_name)
        plens.wait_until_created('source.dat',mute=True)
        src_pos_filename='srcguess.dat'
        os.system('mv source.dat '+src_pos_filename)
        return src_pos_filename


#Later thinking about having mode than 1 shapemodel potential........
def get_circle_RA_DEC(region_name):
        RADEC=[] 
        with open(region_name, 'r') as f:
           for line in f:
              #print(line)
              if line[0:6]=='circle' : #circle(3.589974586,-30.39219944,0.473") # text={47p1}

                 tmp=line.split('circle(')[1].split(')')[0]
                 splitted_line=tmp.split(',')
                 RADEC.append([splitted_line[0],splitted_line[1]]) #RA,DEC in that order
                 try : # NOT USED YET in the code      name_text carry the ID of the circle
                    #print(line.split('text={'))
                    name_text=line.split('text={')[1].split('}')[0]
                 except:
                    name_text='the_name' #This is not clear from the reader make a verbose that no name where taken if we happen to use it
        if len(RADEC)==0:
            print('  ')
            print('ERROR: The region file do not contain any circle ')
            print('stop')

        return RADEC#,name_text



#dRA,dDEC are the source plan position of the shapemodel potentiel
#def build_shape_model_and_launch(lens_model_name,im_name_cropped,im_name_mask,dRAdDEC,z_source,output_name,nRA,nDEC,dxy,n_pix,seeing,verbose):
def build_shape_model_and_launch(lens_model_name,im_name_cropped,im_name_mask,output_source_name,dRAdDEC,z_source,seeing,psf_name,lenstool_name,verbose):
        if verbose>=2:
           ver_pylenstool=True
        else:
           ver_pylenstool=False
        pf=plens.lenstool_param_file(lens_model_name)

        pf.clean()
        pf.set_parameter('runmode','inverse   3 0.200000 200',verbose=False)
        if seeing=='default':
            pf.set_parameter('observation','seeing       0 ',verbose=False)        
        else:
            pf.set_parameter('observation','seeing       1 '+seeing,verbose=False)        

        pf.set_parameter('image','newton 1',verbose=False)
       
        for i,line in zip(range(1,len(dRAdDEC)+1),dRAdDEC):
           i=str(i)
           dRA=str(line[0])
           dDEC=str(line[1])
           pf.set_parameter('shapelimit '+i,'s_sigx 1 0.002 0.99 0.01',verbose=False)
           pf.set_parameter('shapelimit '+i,'s_sigy 1 0.002 0.99 0.01',verbose=False)
           pf.set_parameter('shapelimit '+i,'mag 1 25 32 0.1',verbose=False)
           pf.set_parameter('shapelimit '+i,'s_angle 1 0 180 0.01',verbose=False)
           
           pf.set_parameter('shapemodel '+i,'id   s'+i,verbose=False)
           pf.set_parameter('shapemodel '+i,'s_center_x  '+dRA,verbose=False)
           pf.set_parameter('shapemodel '+i,'s_center_y  '+dDEC,verbose=False)
           pf.set_parameter('shapemodel '+i,'s_angle      90.0',verbose=False)
           pf.set_parameter('shapemodel '+i,'s_sigx 0.5',verbose=False)
           pf.set_parameter('shapemodel '+i,'s_sigy 0.5',verbose=False)
           pf.set_parameter('shapemodel '+i,'mag 25',verbose=False)
           pf.set_parameter('shapemodel '+i,'type 4',verbose=False)
           pf.set_parameter('shapemodel '+i,'index 1',verbose=False)
        
        pf.remove_section('cleanlens',verbose=True) 
        pf.set_parameter('cleanlens','cleanset  2 '+str(z_source),verbose=False)
        pf.set_parameter('cleanlens','imframe 3 '+im_name_cropped,verbose=False)
        pf.set_parameter('cleanlens','wframe 3 '+im_name_mask,verbose=False)
        pf.set_parameter('cleanlens','pixel 0.2',verbose=False) #Help define the size of the pixel especially needed if it is not squre or if there is an angle
        pf.set_parameter('cleanlens','sframe '+output_source_name,verbose=False)
        if psf_name!='default':
           pf.set_parameter('cleanlens','psfframe '+psf_name,verbose=False)
        
        os.system('rm bestopt.par')
        pf.launch_model(name='pylenstool_shapemodel.par',screen=True,OMP_NUM_THREADS='default',launching_name_of_lenstool=lenstool_name,verbose=ver_pylenstool)

def make_prediction(output_name,output_source_name,nRA,nDEC,dxy,n_pix,dRAdDEC,z_source,lenstool_name,verbose):
        if verbose>=2:
           ver_pylenstool=True
        else:
           ver_pylenstool=False
        plens.wait_until_created('bestopt.par',mute=True) #I rm it the step before
        pf=plens.lenstool_param_file('bestopt.par')
        pf.clean()
        pf.zoom_window(nRA,nDEC,dxy,verbose=False)
        pf.set_parameter('runmode','pixel 1 '+str(n_pix)+' '+output_name,verbose=False)
        pf.remove_section('cleanlens',verbose=True)
        pf.set_parameter('cleanlens','cleanset  2 '+str(z_source),verbose=False)
        pf.set_parameter('cleanlens','pixel 0.2',verbose=False) #Help define the size of the pixel especially needed if it is not squre or if there is an angle

        ###  source plane image of the model
        s_n=1000
        size=100
        pf.set_parameter('cleanlens','sframe '+output_source_name,verbose=False)
        RAref,DECref=pf.get_ref()
        RAref=float(RAref)
        DECref=float(DECref)
        xmin=dRAdDEC[0][0]-float(size)/2.0
        ymin=dRAdDEC[0][1]-float(size)/2.0
        xmax=xmin+float(size)
        ymax=ymin+float(size)
        pf.set_parameter('cleanlens','s_n   '+str(s_n),verbose=False)
        pf.set_parameter('cleanlens','s_xmin '+str(xmin),verbose=False)
        pf.set_parameter('cleanlens','s_xmax '+str(xmax),verbose=False)
        pf.set_parameter('cleanlens','s_ymin '+str(ymin),verbose=False)
        pf.set_parameter('cleanlens','s_ymax '+str(ymax),verbose=False)


        pf.set_parameter('source','z_source '+str(z_source),verbose=False)
        #os.system('rm source.fits')
        pf.launch_model(name='pylenstool_shapemodel_prediction.par',screen=True,OMP_NUM_THREADS='default',launching_name_of_lenstool=lenstool_name,verbose=ver_pylenstool)
        #plens.wait_until_created('source.fits',mute=True) #I rm it the step before
        #os.system('mv source.fits '+output_source_name)

def launch_shape_model(lens_model_name,region_name,z_source,im_name_cropped,im_name_mask,img_pixel_size,center,FOV_im,seeing,psf_name,lenstool_name,verbose):
        os.system('rm bayes.dat')
        if verbose>=2:
           print('I removed possibly existing bayes.dat because there is a conflict while guessing image position')
        dRAdDEC=compute_source_position(lens_model_name,region_name,z_source,lenstool_name,verbose)
        if verbose>=2:
           print('The relative source positions are (RA, DEC in delta_arcsec)',dRAdDEC)
       
        name=get_name_from_poly(region_name)
        output_name='lensed_shapemodel_'+name+'.fits'
        output_source_name='source_shapemodel_'+name+'.fits'
        nRA,nDEC,dxy,n_pix=best_guess_from_img_rec(lens_model_name,im_name_cropped,img_pixel_size,center,FOV_im,z_source,lenstool_name,verbose)

        
        build_shape_model_and_launch(lens_model_name,im_name_cropped,im_name_mask,output_source_name,dRAdDEC,z_source,seeing,psf_name,lenstool_name,verbose)
        make_prediction(output_name,output_source_name,nRA,nDEC,dxy,n_pix,dRAdDEC,z_source,lenstool_name,verbose)

def fits_to_lenstool_image_file(image_file_name):
   print('image_file_name',image_file_name)
   hdu_list = fits.open(image_file_name)   #open the file
   w = wcs.WCS(hdu_list[0].header)       #get the WCS out of it
   n_x,n_y=np.shape(hdu_list[0].data)    #record the lenght in pixel of the image 
   pixcrd = np.array([[0, 0], [0, n_y-1], [n_x-1, 0],[n_x-1, n_y-1]], np.float_)  # get the array of the corner of the image
   world = w.wcs_pix2world(pixcrd, 1)  #The the corner into RA and DEC
   return world


def write_the_file(tab,output_filename,z_source):
    new_file = open(output_filename,'w')
    for i,line in zip(range(0,len(tab)),tab) :
        string=str(i+1)+' '+str(line[0])+' '+str(line[1])+' 0.1 0.1 0.0 '+str(z_source)+' 0.0'
        new_file.write(string+'\n')
    new_file.close()


#I use this function to lens back the source
#def get_image_lensed(lens_model_name,source_plane_image_name,z_source,output_name='lensed_img.fits',resolution='1000'):
def best_guess_from_img_rec(lens_model_name,im_name_cropped,img_pixel_size,center,FOV_im,z_source,lenstool_name,verbose):  #reso is the resolution of the image you want to compare with, its gonna be twice more resolution
     if verbose>=2:
        ver_pylenstool=True
     else:
        ver_pylenstool=False
     pf=plens.lenstool_param_file(lens_model_name)
     pf.remove_section('cleanlens',verbose=False) 
     if FOV_im=='default' or center=='default':
        tab_coord=fits_to_lenstool_image_file(im_name_cropped)
        name_tmp_img_guess='tmp_img_guess.dat'
        write_the_file(tab_coord,name_tmp_img_guess,z_source)  #Is gonna create the file that contain the corner of the image then it can be lensed
        os.system('rm image.all')
        pf.clean()
        pf.set_parameter('runmode','image 1 '+name_tmp_img_guess,verbose=False)
        pf.zoom_window(0,0,200,verbose=False)
        pf.launch_model(name='pylenstool_guess_image_plan_size.par',launching_name_of_lenstool=lenstool_name,verbose=ver_pylenstool)
        plens.wait_until_created('image.all')
        aaa=np.loadtxt('image.all')
     if FOV_im!='default':
        FOV_im=int(FOV_im)
     else:
        FOV_im=np.round(1.5*np.max([np.max(aaa[:,1])-np.min(aaa[:,1]),np.max(aaa[:,2])-np.min(aaa[:,2])]))

     #print('CENTERRRRRR',center)
     if center=='default':
        RA=np.mean(aaa[:,1]) #There are in arcsec ref3 mode
        DEC=np.mean(aaa[:,2]) #There are in arcsec ref3 mode
        RAref,DECref=pf.get_ref()
        RA,DEC=plens.ref3_to_deg(float(RAref),float(DECref),RA,DEC) #to put them in DEG
     elif center[0]=='ref':
        RA,DEC=pf.get_ref()
        RA=float(RA)
        DEC=float(DEC)
     else:
        RA=float(center[0])
        DEC=float(center[1])
     n_pix=int(FOV_im/img_pixel_size)
     if verbose>=2:
        print('The size of the image frame is {}'.format(str(FOV_im)))
        print('On a {} by {} pixel grid'.format(str(n_pix),str(n_pix)))
        print('The resolution will be {}'.format(str(img_pixel_size)))
     return RA,DEC,FOV_im,n_pix

#This is the master function
def optimize_source_shape(im_name,region_name,lens_model_name,z_source,
                          img_pixel_size=0.1,center='default',FOV_im='default',seeing='default',psf_name='default',lenstool_name='lenstool',verbose=2):
        print('verbose',verbose)
        im_name_cropped,im_name_mask=cut_and_mask(im_name,region_name,verbose)
        #print(im_name_cropped,im_name_mask)
        launch_shape_model(lens_model_name,region_name,z_source,im_name_cropped,im_name_mask,img_pixel_size,center,FOV_im,seeing,psf_name,lenstool_name,verbose)
        #lens_model_name=

if __name__== "__main__":

        im_name=sys.argv[1]
        poly_name=sys.argv[2]
        verbose=2
        #mask_name='mask_47p1.fits'
        im_name_cropped,im_name_mask=cut_and_mask(im_name,poly_name,verbose)




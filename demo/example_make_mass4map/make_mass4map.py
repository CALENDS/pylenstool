import pylenstool as plens


#First step loading the parameter file
pf=plens.lenstool_param_file('my_training_model.par')

z_cluster=pf.get_zlens()

print(z_cluster)

pf.clean()

pf.set_parameter('runmode','mass 4 1000 '+z_cluster[0]+' mass4.fits')

pf.zoom_window(0,0,80)

pf.launch_model(screen=True,OMP_NUM_THREADS='2',name='pylenstool_model_m.par')
        



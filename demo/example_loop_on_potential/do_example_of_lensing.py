import pylenstool as plens
import numpy as np

pf=plens.lenstool_param_file('best_ref.par') # Loads the parameter named 'best_ref.par'

pf.keep_only_some_potentials(['O1']) # Selects in the parameter file only the potential listed, here 'O1'
pf.set_parameter('runmode','image 0') # In the runmode section, sets the image parameter to 0
pf.set_parameter('runmode','source 1 source.dat') # In the runmode section, sets the source parameter to 1 and give the name of the ouput as source.dat
#This loop will change the velocity dispersion of the potential (to increase it mass), set parameter to create a fits image of a lensed source and launch a model.
for velo in np.arange(100.0,2000.0,100):  # Loop from 100 km/s to 2000 km/s by step of 100

        pf.set_parameter('runmode','pixel 1 3500 '+str(int(velo))+'_simu.fits') # In the runmode section set pixel parameter to produce a lensed images for each realisation of the model
        pf.set_parameter('potentiel     O1','v_disp     '+str(velo)) # In the potentiel O1 change the v_disp parameter to a new value
        pf.launch_model()  # write/launch the corresponding lenstool model





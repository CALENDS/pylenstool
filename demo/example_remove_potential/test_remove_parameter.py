import pylenstool as plens

pf=plens.lenstool_param_file('test_remove_parameter.par')

pf.remove_potential(['O3'])

pf.write_param_file(check_count_lens=False,verbose=False)  #This will produce a file named pylenstool_model.par



import pylenstool as plens

pf=plens.lenstool_param_file('test_remove_parameter.par')
pf.remove_parameter('runmode','source',verbose=True)

pf.remove_parameter('runmode','source',verbose=True) # if you do it  twice, it will tell you that there is not source
#pf.write_param_file('trash.par')

pf.remove_parameter('runmode','ampli',verbose=True) #It will remove the third occurence
pf.remove_parameter('image','z_m_limit',removeid=3,verbose=True) #It will remove ALL the  occurence

pf.remove_parameter('runmode','ampli',removeid=3,verbose=True) #Should tell you that it did not find the occurence


pf.remove_section('cosmologie',verbose=True) 

pf.remove_section('shapemodel',removeid=3,verbose=True) #It will remove the third occurence

pf.write_param_file(check_count_lens=False,verbose=False)  #This will produce a file named pylenstool_model.par




# Tutorial for basic functionalities:

import pylenstool as plens

# Load a lenstool parameter file (test.par as an example) as pf 

pf=plens.lenstool_param_file('unit_test.par')

# Change one parameter in image section of the parameter file

pf.set_parameter('image','z_m_limit 8 60 1 0.5 6.0 0.1')

# If a parameter does not exist it will be created

pf.set_parameter('image','critic      1 -8.81883 22.8604 6.6 0.05 3.98')

# If the section does not exist it will be created

pf.set_parameter('mode_that_not_exist','critic      1 -8.81883 22.8604 6.6 0.05 3.98')

# the function set_parameter()  needs a keyword e.g. 'cline' and the paramter you want to put inside e.g. 'critic      1 -8.81883 22.8604 6.6 0.05 3.98'


# print all the different ''mode'' you have 

pf.get_mode()

# write the parameter file into a file named demo.par

pf.write_param_file(output_filename='demo.par')

# Regardless if you write it or not you could launch the model, code launch it like 'lenstool demo.par' 

pf.launch_model()

# You might want to launch it in a 'screen', it's doable, put the option



# contact email : guillaume.mahler@univ-lyon1.fr - 

